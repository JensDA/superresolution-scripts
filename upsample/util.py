import logging
import os
import os.path as osp
from shutil import disk_usage
from enum import Enum
from datetime import datetime
from glob import glob
try:
    from natsort import os_sorted
except Exception:
    print("WARNING: natsorted not found. Using plain sorted instead.")
    os_sorted = sorted


image_exts = ['.png', '.tif', '.tiff', '.jpg', '.jpeg', '.webp', '.jp2', '.bmp', '.heif', '.avif',
              '.flif', '.gif', '.jxl', '']

video_exts = ['.avi', '.mp4', '.mpeg', '.mpg', '.m4v', '.mkv', '.3gp', '.rm', '.ts', '.wmv',
              '.asf', '.webp', '.gif', '.mov', '.flv', '.mp2', '.m2v', '.webm', '.rmvb', '.rm']

sizeUnits = {
    'Byte': 0,
    'kiB':  1,
    'MiB':  2,
    'GiB':  3
}

MAX_ALLOWED_TILE_LENGTH = {1: 1920, 2: 1080, 4: 720}
#MAX_ALLOWED_TILE_LENGTH = {1: 999999, 2: 999999, 4: 999999}

FRAME_INDEX_LENGTH = 7
FRAME_INDEX_STR = '0{:d}d'.format(FRAME_INDEX_LENGTH)
FRAME_INDEX_FORMAT_STR = '{:' + '{:s}'.format(FRAME_INDEX_STR) + '}'


class ReturnCode(Enum):
    SUCCESS = 0
    SUCCESS_EXTRACTION = 1

    SKIPPED = 10
    SKIPPED_ALREADY_PROCESSED = 11
    SKIPPED_TOO_BIG = 12
    SKIPPED_WRONG_TYPE = 13
    SKIPPED_USER_REQUESTED = 14

    ERROR = 100
    ERROR_UNRECOVERABLE = 1000
    ERROR_INSUFFICIENT_DISK_SPACE = 1001

    def is_error(returnCode):
        return returnCode.value >= ReturnCode.ERROR.value

    def is_recoverable_error(returnCode):
        is_recoverable = returnCode.value < ReturnCode.ERROR_UNRECOVERABLE.value
        return ReturnCode.is_error(returnCode) and is_recoverable


RETURN_CODE_STR = {
    ReturnCode.SUCCESS: 'Success',
    ReturnCode.SUCCESS_EXTRACTION: 'Success (extraction only)',

    ReturnCode.SKIPPED: 'Skipped',
    ReturnCode.SKIPPED_ALREADY_PROCESSED: 'Skipped: Already processed',
    ReturnCode.SKIPPED_TOO_BIG: 'Skipped: Input too big',
    ReturnCode.SKIPPED_WRONG_TYPE: 'Skipped.:Wrong input file type',
    ReturnCode.SKIPPED_USER_REQUESTED: 'Skipped: Requested by user.',

    ReturnCode.ERROR: 'Error',
    ReturnCode.ERROR_UNRECOVERABLE: 'Fatal Error',
    ReturnCode.ERROR_INSUFFICIENT_DISK_SPACE: 'Fatal Error: Not enough disk space',
}


############# utility functions ######################

def get_timestamp():
    return datetime.now().strftime('%y%m%d-%H%M%S')


def setup_logger(logger_name, root, phase, level=logging.INFO, screen=False, tofile=False):
    '''set up logger'''
    lg = logging.getLogger(logger_name)
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)03d - %(levelname)s{}: %(message)s'.format(phase),
        datefmt='%y-%m-%d %H:%M:%S' )
    lg.setLevel(level)
    if tofile:
        log_file = os.path.join(root, phase + '_{}.log'.format(get_timestamp()))
        fh = logging.FileHandler(log_file, mode='w')
        fh.setFormatter(formatter)
        lg.addHandler(fh)
    if screen:
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        lg.addHandler(sh)


def is_image(filename):
    ext = osp.splitext(filename)[1].lower()
    return ext in image_exts


def is_video(filename):
    ext = osp.splitext(filename)[1].lower()
    return ext in video_exts


def get_temp_dir(filepath, baseDir=''):
    path, filename = osp.split(filepath)

    if baseDir != '':
        path = baseDir
    path = osp.realpath(path)

    fileroot = get_filePathStub(filename)
    file_id = fileroot\
        .replace('[', '')\
        .replace(']', '')\
        .replace('%20', '_')\
        .replace(' ', '_')

    tempDir = osp.join(path, 'tmp_{:s}'.format(file_id))

    if not osp.exists(tempDir):
        os.makedirs(tempDir)
    return tempDir


def get_model_name(model, opt):
    model_name = ''

    doESRGAN    = opt['doESRGAN']
    doDeComp    = opt['doDeComp']
    DeCompModel = opt['DeCompModel']

    if doESRGAN and model != DeCompModel:
        sr_model_name = get_fileNameStub(model)
        model_name += '_SR_{}'.format(sr_model_name)
    if doDeComp:
        deComp_model_name = get_fileNameStub(DeCompModel)
        model_name += '_dC_{}'.format(deComp_model_name)
    if not (doESRGAN or doDeComp):
        model_name = '_reencode'

    return model_name


def escape_filename_ffmpeg(filename):
    notEscapedApostrophe = "'"
    escapedApostrophe = "'\\''"

    if notEscapedApostrophe in filename and not (escapedApostrophe in filename):
        filename = filename.replace(notEscapedApostrophe, escapedApostrophe)

    return filename


def get_output_name(filename, model='', opt=None, size_str='', custom_suffix='',\
                    ext='.tif', ffmpeg_escaped=False, customPath=None):
    if ffmpeg_escaped:
        filename = escape_filename_ffmpeg(filename)

    file_path_root = get_filePathStub(filename)

    suffix = custom_suffix + ext    # NOTE keep suffix at the end

    if size_str != '':
        suffix = '_{}{}'.format(size_str, suffix)

    if model != '':
        suffix = '{}{}'.format(get_model_name(model, opt), suffix)

    out_name = '{}{}'.format(file_path_root, suffix)

    while osp.exists(out_name):
        suffix = '{}{}'.format('_new', suffix)
        out_name = '{}{}'.format(file_path_root, suffix)

    if customPath:
        assert osp.isdir(customPath), customPath
        out_name = get_new_fileNamePath(out_name, customPath)

    return out_name


def sanitize_file_name(fileName):
    """ Returns the file name with colons (:) replaced by underscores (_). """
    sanitizedFileName = fileName.replace(':', '_')
    return sanitizedFileName


def bytes_to(byte, exp):
    """ Returns the input byte value converted to a bigger unit according to IEC 80000-13,
        e.g. KiB, MiB, GiB """
    for e in range(exp):
        byte /= 1024
    return byte


def makeDivisible(inputNumber, divisor):
    """ Returns the largest multiple of the divisor that is equal to or less than
        the input number. """
    return int( (int(inputNumber) // divisor) * divisor )


def get_filePathStub(filePath):
    """ Returns the file path without the file extension.
        e.g. '/home/user/the_path/0000001.tif' -> '/home/user/the_path/0000001' """
    return osp.splitext(filePath)[0]


def get_fileExtension(filePath):
    """ Returns the file extension including the period.
        e.g. '/home/user/the_path/0000001.tif' -> '.tif' """
    return osp.splitext(filePath)[1].lower()


def get_fileNameStub(fileName):
    """ Returns the file name with neither path nor extension.
        e.g. '/home/user/the_path/0000001.tif' -> '0000001' """
    return get_filePathStub( osp.basename(fileName) )


def get_filePath(fileName):
    """ Returns the file's path.
        e.g. '/home/user/the_path/0000001.tif' -> '/home/user/the_path' """
    filePath = osp.dirname(fileName)
    assert osp.isdir(filePath)
    return filePath


def get_new_fileNamePath(fileNamePath, newPath):
    """ Returns the file name in the new path.
        e.g. '/home/user/the_path/0000001.tif' -> '/mnt/newPath/0000001.tif'  """
    newFileNamePath = osp.join(newPath, osp.basename(fileNamePath))
    return newFileNamePath


def floatEqual(n1, n2, epsilon=0.001):
    """ Returns True if the distance between n1 and n2 is less than epsilon. """
    return abs(n1 - n2) < epsilon


def get_free_space_Bytes(fileName):
    path = get_filePath(fileName)
    _, _, freeBytes = disk_usage(path)
    return freeBytes


def check_free_space_GiB(fileName, limit_GiB):
    freeBytes = get_free_space_Bytes(fileName)
    return bytes_to(freeBytes, sizeUnits['GiB']) > limit_GiB


def is_aspect_ratio_more_narrow(r_image, r_screen):
    if r_screen < 1:
        r_screen = 1 / r_screen
    if r_image < 1:
        r_image = 1 / r_image

    return r_image > r_screen


def autoSelectSmallerModel(opt, args, width, height, ext):
    logger = logging.getLogger('base')
    logger.debug((args, width, height, ext))

    force2x = False
    if args.sr1x:
        logger.info('sr1x')
        return 1
    elif args.sr2x:
        logger.info('sr2x')
        return 2
    elif args.sr4x:
        logger.info('sr4x')
        return 4
    elif ext in opt['smallerModelExtensions']:
        logger.info('ext -> sr2x')
        force2x = True  # may use 1x model if input is large enough

    using1xmodel, using2xmodel = args.sr1x, args.sr2x
    has2xmodel = 'sr2x_model' in opt
    has1xmodel = 'sr1x_model' in opt

    if opt['doAutoUseSmallerModel'] and not (args.sr1x or args.sr2x):
        limit = opt['auto2xModelResThreshold'] / opt['sr_Scale']
        limit /= opt['in_preScale']
        if (width > limit and height > limit) or force2x:
            using2xmodel = True
            logger.debug((limit, using2xmodel))
            if has1xmodel or opt['skip1x']:  # skip1x is done by caller later, if using1xmodel set
                limit = opt['auto1xModelResThreshold'] / opt['sr_Scale']
                logger.debug("limit({}) = auto1xModelResThreshold({}) / sr_Scale({})".format(
                    limit, opt['auto1xModelResThreshold'], opt['sr_Scale']))
                limit /= opt['in_preScale']
                if width > limit and height > limit:
                    using1xmodel = True
                    using2xmodel = False
            logger.debug((limit, using2xmodel))
    logger.debug(using2xmodel)

    using2xmodel = using2xmodel and has2xmodel and not opt['doMultiSR']
    using1xmodel = using1xmodel and has1xmodel and not opt['doMultiSR']
    assert not (using1xmodel and using2xmodel)

    if using1xmodel:
        modelSelection = 1
    elif using2xmodel:
        modelSelection = 2
    else:
        modelSelection = 4
    logger.info('sr{}x'.format(modelSelection))

    return modelSelection


def stringToBool(boolean):
    return "True" if boolean else "False"


def optimize_tile_length(tileLength, imageSize, scale):
    """ Set tile length to maximum image length if total number of pixels is less than
        tileLength squared. """
    logger = logging.getLogger('base')
    imgMaxLength = max(imageSize[0], imageSize[1])

    if imgMaxLength > tileLength and imageSize[0] * imageSize[1] <= tileLength * tileLength:
        optimizedTileLength = imgMaxLength #min(imgMaxLength, MAX_ALLOWED_TILE_LENGTH[scale])
        logger.info( 'optimized tile length: {} -> {}'.format(tileLength, optimizedTileLength) )
    else:
        optimizedTileLength = tileLength
        logger.debug( 'tile length: {}'.format(optimizedTileLength) )
    return optimizedTileLength


def copy_missing_frames(path, ext, alternate=False):
    """ Searches for missing frames in path and links them to fill up the range """
    print('copy_missing_frames...')
    files = glob(osp.join(path, '*{}'.format(ext)))
    files = os_sorted(files)

    addedFrames = 0
    donePrev = False
    for i, frame in enumerate(files):
        frameNr = int(get_fileNameStub(frame))
        expectedFrameNr = i+1+addedFrames
        if frameNr > expectedFrameNr:
            diff = frameNr - expectedFrameNr
            addedFrames += diff
            if alternate:
                if donePrev:
                    donePrev = False
                    continue
                else:
                    donePrev = True
            print(frame, expectedFrameNr, diff, addedFrames)
            frameNrToCopy = expectedFrameNr-1
            formatStr = FRAME_INDEX_FORMAT_STR + '{}'
            frameToCopy = osp.join(path, formatStr.format(frameNrToCopy, ext))
            for j in range(expectedFrameNr, frameNr):
                target = osp.join(path, formatStr.format(j, ext))
                print(frameToCopy, target)
                os.symlink(frameToCopy, target)
        else:
            assert frameNr == expectedFrameNr, (frameNr, expectedFrameNr)
    print('copy_missing_frames done.')