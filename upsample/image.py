import logging
from math import floor
import os.path as osp
from multiprocessing import Process, JoinableQueue, RLock
from queue import Empty

import upsample.util as upUtil
import upsample.model as upModel

# needs to be imported after upsample model
from wand.image import Image as wImage
from wand.drawing import Drawing as wDrawing

# Wand profiles workaround: https://github.com/emcconville/wand/issues/440
from wand.api import library


library.MagickProfileImage.argtypes = library.MagickSetImageProfile.argtypes
library.MagickProfileImage.restype = library.MagickSetImageProfile.restype
IMG_RESIZE_GAMMA = 2.2


def profileImage(img, profile_name, profile_body):
    """Sets image profile by using `MagickProfileImage`, and not
    `MagickSetImageProfile`.
    """
    status = True
    if profile_body is not None:
        status = library.MagickProfileImage(img.wand,
                                            profile_name,
                                            profile_body,
                                            len(profile_body))
    if bool(status) is False:
        img.raise_exception()
    return status


############# image functions ########################
def open_image_wand(filepath, colorspace, depth):
    return wImage(filename=filepath, colorspace=colorspace, depth=32)


def open_image(filepath, opt):
    logger = logging.getLogger('base')
    if opt['in_convertToSRGB']:
        colorspace = 'srgb'
    elif opt['in_ColorspaceOverride'] != '':
        colorspace = opt['in_ColorspaceOverride']
    else:
        colorspace = None

    logger.debug( 'Opening image with colorspace "{}"'.format(colorspace) )

    img = open_image_wand(filepath, colorspace, 32)
    img.depth = 32
    # img.colorspace is now sRGB/ycbcr

    img.alpha_channel = 'remove'

    logger.debug( 'Image colorspace "{}"'.format(img.colorspace) )
    if 'icc' in dict(img.profiles):
        logger.info('Image has ICC profile.')
    return img


def convert_to_colorspace(img, target_colorspace):
    logger = logging.getLogger('base')

    if img.colorspace != target_colorspace:
        logger.info( '{} -> {}'.format(img.colorspace, target_colorspace) )
    else:
        logger.debug( '{} -> {}'.format(img.colorspace, target_colorspace) )

    img.transform_colorspace(target_colorspace)

    return img


def convert_to_linearRGB(img):
    return convert_to_colorspace(img, 'rgb')


def convert_to_sRGB(img):
    return convert_to_colorspace(img, 'srgb')


def deGamma(img):
    img.gamma(1./IMG_RESIZE_GAMMA)
    return img


def reGamma(img):
    img.gamma(IMG_RESIZE_GAMMA)
    return img


def resize_image(img, resizeFilter, opt):
    logger = logging.getLogger('base')
    factor = opt['in_preScale']

    outImg = img.clone()

    width  = img.width
    height = img.height
    if opt['in_preCropBox'] and resizeFilter == 'box':
        w_crop = width  % (1/factor)
        h_crop = height % (1/factor)
        if w_crop > 0 or h_crop > 0:
            width  = floor(width  - w_crop)
            height = floor(height - h_crop)
            outImg.crop(right=width, bottom=height)
            logger.info('pre-crop: w({}->{}) h({}->{})'.format(
                img.width, width, img.height, height)
            )

    w = floor(width  * factor)
    w = upUtil.makeDivisible(w, 2)
    h = floor(height * factor)
    h = upUtil.makeDivisible(h, 2)

    logger.debug('new size: {}x{}'.format(w, h))

    assert(outImg.depth >= 16), outImg.depth
    outImg = deGamma(outImg)
    logger.info( 'Scaling image with factor {} and filter \"{}\"'.format(factor, resizeFilter) )
    if resizeFilter != 'scale':
        outImg.resize(w, h, resizeFilter)
    else:
        outImg.scale(w, h)
    outImg = reGamma(outImg)

    return outImg


def crop_image(img, opt):
    ''' crops image into one or more tiles, column based '''
    logger = logging.getLogger('base')
    scale = opt['sr_Scale']
    tile_length = opt['tile_length'][scale]
    if opt['model_type'] != 'tensorrt':
        tile_length = upUtil.optimize_tile_length(tile_length, img.size, scale)
    tile_overlap = opt['tile_overlap']

    tiles = []
    maxlength = tile_length
    maxtiles = 99   # in one dimension

    h = img.height
    w = img.width
    w_tiles = 0
    h_tiles = 0

    # determine number of tiles needed
    for i in range(1, maxtiles):
        if maxlength + (maxlength-tile_overlap) * (i-1) >= w:
            w_tiles = i
            break
    for i in range(1, maxtiles):
        if maxlength + (maxlength-tile_overlap) * (i-1) >= h:
            h_tiles = i
            break
    logger.debug({'w_tiles': w_tiles, 'h_tiles': h_tiles})

    if w_tiles == 0 or h_tiles == 0:     # no tiles (i.e. > maxtiles)
        logger.error('Size Exception! Skipping.')
    elif w_tiles == 1 and h_tiles == 1:  # one tile
        tiles.append(img)
        logger.info('crop({}*{}): Single tile.'.format(w, h))
    else:  # more than one tile
        logger.info('crop({}*{}): {}*{} tiles.'.format(w, h, w_tiles, h_tiles))
        for i_w in range(w_tiles):
            left = i_w * tile_length - i_w * tile_overlap
            right = min(left + tile_length, w)
            logger.debug({'w': w, 'left': left, 'right': right})

            for i_h in range(h_tiles):
                top = i_h * tile_length - (i_h * tile_overlap)
                bottom = min(top + tile_length, h)
                logger.debug({'h': h, 'top': top, 'bottom': bottom})

                tiles.append(img[left:right, top:bottom])

    return tiles, {'w': w_tiles, 'h': h_tiles}


def restore_tiled_image(tiles, tileLayout, imgMetaInfo, opt, noProfiles=False):
    logger = logging.getLogger('base')
    assert len(tiles) > 1

    sr_Scale = opt['sr_Scale']
    size = imgMetaInfo['size']
    tile_length = opt['tile_length'][sr_Scale]
    if opt['model_type'] != 'tensorrt':
        tile_length = upUtil.optimize_tile_length(tile_length, size, sr_Scale)
    out_CompositeOperator = opt['out_CompositeOperator']

    overlap = opt['tile_overlap'] * sr_Scale
    w_max = tileLayout['w'] - 1
    h_max = tileLayout['h'] - 1

    restoredWidth  = size[0] * sr_Scale
    restoredHeight = size[1] * sr_Scale
    restoredDepth  = 32     # changed below

    colorspace = 'srgb' if opt['in_convertToSRGB'] else imgMetaInfo['colorspace']
    restoredImage = wImage(width=restoredWidth, height=restoredHeight, depth=restoredDepth,
                           colorspace=colorspace)
    if not noProfiles:
        restoredImage.profiles = imgMetaInfo['profiles']  # <- does NOT work
        #print(restoredImage.profiles.get('exif'))
        for key in imgMetaInfo['profiles'].keys():
            logger.info('Profile: {}'.format(key))
            profileImage(restoredImage, bytes(key, 'UTF-8'), imgMetaInfo['profiles'][key])
        #print(restoredImage.profiles.get('exif'))

    i_w = i_h = 0
    for tile_img in tiles:
        t_left = i_w * tile_length * sr_Scale - (i_w * overlap)
        t_top  = i_h * tile_length * sr_Scale - (i_h * overlap)
        logger.debug('Drawing tile {:02d}x{:02d}'.format(i_w+1, i_h+1))

        t_width  = tile_img.width
        t_height = tile_img.height
        logger.debug('(l:{:d}, t:{:d}, w:{:d}, h:{:d})'.format(t_left, t_top, t_width, t_height))

        # crop tile overlap to hide seams
        firstRow    = i_h == 0
        lastRow     = i_h == h_max
        firstColumn = i_w == 0
        lastColumn  = i_w == w_max
        logger.debug('firstRow:{:b} lastRow:{:b} firstCol:{:b} lastCol:{:b}'.format(
            firstRow, lastRow, firstColumn, lastColumn ))
        l_crop = overlap // 2

        # default: crop all sides
        t_width  -= 2 * l_crop  # crop left and right
        t_height -= 2 * l_crop  # crop top and bottom
        t_top  += l_crop        # move down on canvas
        t_left += l_crop        # move right on canvas
        crop_top  = l_crop      # shift crop window to vertical center
        crop_left = l_crop      # shift crop window to horizontal center

        if firstRow:
            t_height += l_crop  # don't crop top
            t_top -= l_crop     # don't move down on canvas
            crop_top = 0        # shift crop window to vertical 0
        if firstColumn:
            t_width += l_crop   # don't crop left
            t_left -= l_crop    # don't move right on canvas
            crop_left = 0       # shift crop window to horizontal 0
        if lastRow:
            t_height += l_crop  # don't crop bottom
        if lastColumn:
            t_width += l_crop   # don't crop right

        logger.debug('{:s}(l:{:d}, t:{:d}, w:{:d}, h:{:d})'.format(
            out_CompositeOperator, t_left, t_top, t_width, t_height) )
        tile_img.crop(top=crop_top, left=crop_left, width=t_width, height=t_height)

        with wDrawing() as draw:
            draw.composite(
                operator=out_CompositeOperator,
                left=t_left,
                top=t_top,
                width=t_width,
                height=t_height,
                image=tile_img
            )
            draw(restoredImage)
            tile_img = None

        i_h += 1
        if i_h >= tileLayout['h']:
            i_h  = 0
            i_w += 1

    assert i_w == tileLayout['w'] and i_h == 0, "i_w={}({}), i_h={}({})".format(
        i_w, tileLayout['w'], i_h, tileLayout['h'])

    depth = 16 if opt['out_forceHighDepth'] or opt['isVideo'] else 8
    restoredImage.depth = depth

    return restoredImage


def save_image_wand(image, fileName, opt):
    if opt['doCompressIntermediates']:
        image.compression = 'zip'
        image.compression_quality = opt['intermediateCompressionLevel'] * 10
    else:
        image.compression = 'no'

    logger = logging.getLogger('base')
    logger.debug( 'Image profiles: {}'.format( dict(image.profiles) ) )

    image.save(filename=fileName)


def save_image(img, fileName, opt):
    logger = logging.getLogger('base')
    logger.info( 'Saving image to \"{}\"...'.format(fileName) )

    save_image_wand(img, fileName, opt)


def modify_image(img, model, opt):
    ''' Calls crop_image, run_model on tiles and restore_tiled_image. Returns image. '''
    logger = logging.getLogger('base')
    logger.info( 'Cropping image...' )

    # Apparently, 2x models need to be able to divide the size by 2:
    if opt['sr_Scale'] == 2:
        if img.width % 2 > 0:
            logger.debug('Cropping width to even length.')
            img.crop(width=img.width-1)
        if img.height % 2 > 0:
            logger.debug('Cropping height to even length.')
            img.crop(height=img.height-1)

    tiles, tileLayout = crop_image(img, opt)
    assert len(tiles) == tileLayout['w'] * tileLayout['h']

    #preTime = time.clock_gettime_ns(time.CLOCK_MONOTONIC)
    for i, tile in enumerate(tiles):
        logger.info( 'Tile {:d}/{:d}...'.format(i+1, len(tiles)) )
        logger.debug( 'pretile: w={} h={}'.format(tiles[i].width, tiles[i].height))
        tiles[i] = upModel.run_model(model, opt, tile)
        logger.debug( 'posttile: w={} h={}'.format(tiles[i].width, tiles[i].height))
    #postTime = time.clock_gettime_ns(time.CLOCK_MONOTONIC)
    #logger.debug( "{} ns".format(postTime - preTime) )

    if len(tiles) > 1:
        logger.info('Restoring tiled image...')
        imgMetaInfo = { 'size': img.size, 'colorspace': img.colorspace, 'profiles': img.profiles }
        img = restore_tiled_image(tiles, tileLayout, imgMetaInfo, opt)
        tiles = None
    else:
        profiles = img.profiles
        img = tiles[0]
        img.profiles = profiles  # <- does NOT work
        for key in profiles.keys():
            profileImage(img, bytes(key, 'UTF-8'), profiles[key])
        img.alpha_channel = 'remove'

        depth = 16 if opt['out_forceHighDepth'] or opt['isVideo'] else 8
        img.depth = depth

    return img


def create_tiles_mp(inQueue, outQueue, stopLock, opt):
    upUtil.setup_logger('create_tiles_mp', '.', '  create_tiles', level=logging.INFO, screen=True,
                        tofile=False)
    logger = logging.getLogger('create_tiles_mp')
    #logger.debug('create_tiles_mp')

    while (not inQueue.empty()) or (not stopLock.acquire(block=False)):
        try:
            packet = inQueue.get(block=True, timeout=0.5)
        except Empty:
            logger.debug('resizedQueue is empty.')
            continue

        if not packet['meta']['duplicate']:
            size = packet['meta']['size']
            width = size[0]
            height = size[1]
            with wImage(blob=packet['image'], format='RGB',
                        width=width, height=height, depth=32) as img:
                # Apparently, 2x models need to be able to divide the size by 2:
                if opt['sr_Scale'] == 2:
                    if width % 2 > 0:
                        logger.debug('Cropping width to even length.')
                        img.crop(width=width-1)
                    if height % 2 > 0:
                        logger.debug('Cropping height to even length.')
                        img.crop(height=height-1)

                tiles, tileLayout = crop_image(img, opt)
                #assert len(tiles) == tileLayout['w'] * tileLayout['h']

                packet['image'] = None
                tiles_size = {}
                for i, tile in enumerate(tiles):
                    tiles[i] = tile.make_blob(format='RGB')
                    tiles_size[i] = tile.size
                packet['tiles'] = tiles
                packet['tiles_size'] = tiles_size
                packet['layout'] = tileLayout
        else:
            logger.info('Skipping duplicate frame.')

        #logger.debug('create_tiles_mp -> tileQueue')
        outQueue.put(packet)
        inQueue.task_done()
    stopLock.release()


def run_model_mp(inQueue, outQueue, stopLock, model, opt):
    upUtil.setup_logger('run_model_mp', '.', '     run_model', level=logging.INFO, screen=True,
                        tofile=False)
    logger = logging.getLogger('run_model_mp')
    #logger.debug('run_model_mp')
    c = 0

    while (not inQueue.empty()) or (not stopLock.acquire(block=False)):
        try:
            packet = inQueue.get(block=True, timeout=0.5)
        except Empty:
            logger.info('tileQueue is empty.')
            continue
        c += 1

        if not packet['meta']['duplicate']:
            tiles, tiles_size = packet['tiles'], packet['tiles_size']
            for i, tile in enumerate(tiles):
                logger.info( 'Frame {}, Tile {:d}/{:d}...'.format(c, i+1, len(tiles)) )
                width, height = tiles_size[i][0], tiles_size[i][1]
                with wImage(blob=tile, format='RGB', width=width, height=height, depth=32) as img:
                    logger.debug( 'pretile: w={} h={}'.format(img.width, img.height))
                    img = upModel.run_model(model, opt, img)
                    logger.debug( 'posttile: w={} h={}'.format(img.width, img.height))
                    packet['tiles'][i] = img.make_blob(format='RGB')
                    packet['tiles_size'][i] = img.size

            logger.debug('model puts frame {} into queue...'.format(c))
        else:
            logger.info('Skipping duplicate frame.')

        outQueue.put(packet)
        inQueue.task_done()
        logger.debug('model frame {} task done.'.format(c))

    logger.debug('model destructs InferenceRunner...')
    # InferenceRunner is deleted when this process ends. Clean up before that.
    upModel.InferenceRunner.destruct()
    logger.debug('model releases stopLock...')
    stopLock.release()
    logger.debug('model released stopLock...')


def stitch_tiles_mp(inQueue, outQueue, stopLock, opt):
    upUtil.setup_logger('stitch_tiles_mp', '.', '  stitch_tiles', level=logging.INFO, screen=True,
                        tofile=False)
    logger = logging.getLogger('stitch_tiles_mp')
    #logger.debug('stitch_tiles_mp')
    c = 0

    while (not inQueue.empty()) or (not stopLock.acquire(block=False)):
        try:
            packet = inQueue.get(block=True, timeout=0.5)
        except Empty:
            #logger.debug('modelOutputQueue is empty.')
            continue
        c += 1

        if not packet['meta']['duplicate']:
            tiles, tiles_size, tileLayout = packet['tiles'], packet['tiles_size'], packet['layout']
            imgMetaInfo = packet['meta']
            if len(tiles) > 1:
                logger.info('Restoring tiled image of frame {}...'.format(c))
                for i, tile in enumerate(tiles):
                    width, height = tiles_size[i][0], tiles_size[i][1]
                    logger.debug('Tile {} size: {}*{}'.format(i, width, height))
                    tiles[i] = wImage(blob=tiles[i], format='RGB', width=width, height=height,
                                      depth=32)
                img = restore_tiled_image(tiles, tileLayout, imgMetaInfo, opt, noProfiles=True)
                packet['image'] = img.make_blob(format='RGB')
                packet['meta']['size'] = img.size
                img = None
            else:
                logger.info('Postprocessing frame {}...'.format(c))
                width, height = tiles_size[0][0], tiles_size[0][1]
                with wImage(blob=tiles[0], format='RGB', width=width, height=height,
                            depth=32) as img:
                    img.colorspace = 'srgb' if opt['in_convertToSRGB'] else imgMetaInfo['colorspace']
                    img.alpha_channel = 'remove'
                    depth = 16 if opt['out_forceHighDepth'] or opt['isVideo'] else 8    # TODO
                    img.depth = depth
                    packet['image'] = img.make_blob(format='RGB')
                    packet['meta']['size'] = img.size
            packet['tiles'] = None
            packet['tiles_size'] = None
            packet['tileLayout'] = None
        else:
            logger.info('Skipping duplicate frame.')

        #logger.debug('stitch_tiles_mp -> stitchedQueue')
        outQueue.put(packet)
        inQueue.task_done()

    logger.debug('stitch_tiles_mp finished.')
    stopLock.release()


def save_image_to(image, fileNamePath, outDir, model, opt,
                  addPrescaleSuffix=True, preScale=1, index=0):
    logger = logging.getLogger('base')
    if outDir != '':
        fileName     = osp.split(fileNamePath)[1]
        fileNamePath = osp.join(outDir, fileName)

    suffix = ''
    if addPrescaleSuffix:
        if preScale != 1:
            suffix += '_{:s}{:d}'.format(
                opt['in_preScaleFilters'][index],
                round(preScale * 100)
            )
    logger.info('suffix: ' + suffix)

    output_name = upUtil.get_output_name(fileNamePath, model=model, opt=opt,
                                         custom_suffix=suffix, ffmpeg_escaped=False,
                                         ext=opt['output_ext'])
    output_name = upUtil.sanitize_file_name(output_name)
    save_image(image, output_name, opt)


def process_image(image, model, fileNamePath, opt, doSaveImage=True, outDir='',
                  addPrescaleSuffix=True, index=0, preScale=1):
    ''' Calls modify_image and saves image [optional]. Returns image or None. '''
    logger = logging.getLogger('base')

    image = modify_image(image, model, opt)

    if doSaveImage:
        save_image_to(image, fileNamePath, outDir, model, opt, addPrescaleSuffix, preScale, index)
        image = None

    logger.info('Image done.')
    return image


def checkImageTooLarge(image, opt):
    largestImageLength = max(image.width, image.height)
    return largestImageLength > opt['maxImageLength']


def maxSizeScale(image, opt):
    largestImageLength = max(image.width, image.height)
    return opt['maxImageLength'] / largestImageLength

######################################################
