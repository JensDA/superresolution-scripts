import logging
#from   functools import partial

import numpy         as np
try:
    import onnxruntime   as ort
    import tensorrt      as trt
    import pycuda.driver as cuda
except Exception as e:
    print(e)

from upsample.archs.rrdbnet_arch import RRDBNet_cp as RRDBNet
import torch
from torch.cuda import amp
from basicsr.utils.img_util import img2tensor
from functools import partial

# needs to be imported after onnxruntime
from   wand.image   import Image   as wImage


############# model functions ########################


def run_onnx(onnxSession, inputData):
    ort_input  = {onnxSession.get_inputs()[0].name: inputData}
    outputData = onnxSession.run(None, ort_input)
    return outputData[0]


def run_tensorRT(engine, inputData, scale):
    outputShape = list(inputData.shape)
    for i in range(2, 4):  # for 2 and 3
        outputShape[i] = inputData.shape[i] * scale
    outputShape = tuple(outputShape)

    with engine.create_execution_context() as context:
        success = context.set_binding_shape(0, inputData.shape)
        assert success

        dtype_input = trt.nptype(engine.get_binding_dtype(0))
        h_input  = cuda.pagelocked_empty(
            trt.volume(context.get_binding_shape(0)), dtype=dtype_input )
        dtype_output = trt.nptype(engine.get_binding_dtype(1))
        h_output = cuda.pagelocked_empty(
            trt.volume(context.get_binding_shape(1)), dtype=dtype_output )
        # Allocate device memory for inputs and outputs.
        d_input  = cuda.mem_alloc(h_input.nbytes)
        d_output = cuda.mem_alloc(h_output.nbytes)
        # Create a stream in which to copy inputs/outputs and run inference.
        stream = cuda.Stream()

        # copy input data to pagelocked buffer
        np.copyto(h_input, inputData.ravel())  # convert input data to flattened array

        # Transfer input data to the GPU.
        cuda.memcpy_htod_async(d_input, h_input, stream)
        # Run inference.
        context.execute_async_v2(
            bindings=[int(d_input), int(d_output)], stream_handle=stream.handle )
        # Transfer predictions back from the GPU.
        cuda.memcpy_dtoh_async(h_output, d_output, stream)
        # Synchronize the stream
        stream.synchronize()

    # Return the host output in the original data shape
    outputData = h_output.reshape(outputShape)
    return outputData


def run_pytorch(model, inputData, device):
    inputData = torch.from_numpy(inputData).to(device, memory_format=torch.channels_last)
    with torch.no_grad():
        with amp.autocast():
            outputData = model(inputData).float().cpu().numpy()
    return outputData


class InferenceRunner():
    """ Provides OnnxSession or TensorRT engine. """

    # static class members
    modelCache = {}  # needs to be deleted before program ends, see InferenceRunner.destruct()
    device = None
    runner = None

    def __init__(self, model, opt):
        self.model = model
        self.opt = opt
        if self.model not in InferenceRunner.modelCache:
            InferenceRunner.updateModel(model, opt)

    @staticmethod
    def destruct():
        InferenceRunner.modelCache = None
        InferenceRunner.device = None
        InferenceRunner.runner = None

    @staticmethod
    def updateModel(model, opt):
        # This import causes pycuda to automatically manage CUDA context creation and cleanup.
        # Do this here to avoid creating contexts for each process that imports this file.
        import pycuda.autoinit
        model_type = opt['model_type']
        logger = logging.getLogger('base')
        logger.debug(InferenceRunner.modelCache)
        if model_type == 'onnx':
            assert isinstance(opt['sr_Scale'], int) \
                   and opt['sr_Scale'] > 0
            if model not in InferenceRunner.modelCache:
                logger.info('Creating OnnxSession for model "{}"...'.format(model))
                InferenceRunner.modelCache = {}
                InferenceRunner.modelCache[model] = create_OnnxSession(model, opt)
            else:
                logger. debug('Using existing OnnxSession for model "{}".'.format(model))
                # drop all else to conserve memory
                thisModel = InferenceRunner.modelCache[model]
                InferenceRunner.modelCache = {}
                InferenceRunner.modelCache[model] = thisModel
            InferenceRunner.runner = run_onnx
        elif model_type == 'tensorrt':
            if model not in InferenceRunner.modelCache:
                logger.info('Creating TensorRT engine for model "{}"...'.format(model))
                InferenceRunner.modelCache = {}
                InferenceRunner.modelCache[model] = create_TensorRTEngine(model)
                #logger.info(self.modelCache)
            else:
                logger.debug('Using existing TensorRT engine for model "{}".'.format(model))
                # drop all else to conserve memory
                thisModel = InferenceRunner.modelCache[model]
                InferenceRunner.modelCache = {}
                InferenceRunner.modelCache[model] = thisModel
            InferenceRunner.runner = partial(run_tensorRT, scale=opt['sr_Scale'])
        elif model_type == 'pytorch':
            if model not in InferenceRunner.modelCache:
                logger.info('Creating pytorch model "{}"...'.format(model))
                InferenceRunner.modelCache = {}
                InferenceRunner.device = torch.device(
                    'cuda' if isinstance(opt['gpu_id'], int) and opt['gpu_id'] >= 0 else 'cpu' )
                print(InferenceRunner.device)
                InferenceRunner.modelCache[model] = create_pytorchModel(model,
                                                                        InferenceRunner.device,
                                                                        opt)
                #logger.info(self.modelCache)
            else:
                logger.debug('Using existing pytorch model "{}".'.format(model))
                # drop all else to conserve memory
                thisModel = InferenceRunner.modelCache[model]
                InferenceRunner.modelCache = {}
                InferenceRunner.modelCache[model] = thisModel
            InferenceRunner.runner = partial(run_pytorch, device=InferenceRunner.device)
        else:
            raise NotImplementedError('Unknown model type: {}'.format(opt['model_type']))
        logger.debug(InferenceRunner.modelCache)

    def run(self, inputData):
        result = InferenceRunner.runner(InferenceRunner.modelCache[self.model], inputData)
        return result


def create_OnnxSession(model, opt):
    logger = logging.getLogger('base')
    logger.info('Creating OnnxSession...')

    gpu_id = opt['gpu_id']
    #print("gpu_id: {}".format(gpu_id))

    #sessionOpt = ort.SessionOptions()
    #sessionOpt.optimized_model_filepath = get_filePathStub(model) + "_optEx" + ".onnx"

    if gpu_id is not None \
       and ((isinstance(gpu_id, int) and gpu_id >= 0) or isinstance(gpu_id, str)):
        logger.info('Using CUDA.')
        providers = [
            (
                'CUDAExecutionProvider', {
                    'device_id': str(gpu_id),
                    'arena_extend_strategy': 'kNextPowerOfTwo',
                    'gpu_mem_limit': 6 * 1024 * 1024 * 1024,
                    'cudnn_conv_algo_search': 'HEURISTIC',  # 'EXHAUSTIVE' ,'HEURISTIC', 'DEFAULT'
                    'do_copy_in_default_stream': True,
                }
            ),
            'CPUExecutionProvider',
        ]
        print(providers)
        #onnxSession = ort.InferenceSession(model, providers=providers, sess_options=sessionOpt)
        onnxSession = ort.InferenceSession(model, providers=providers)
    else:
        onnxSession = ort.InferenceSession(model)

    logger.info("OnnxSession for model {:s} created.".format(model))
    return onnxSession


def create_TensorRTEngine(model):
    logger = logging.getLogger('base')
    logger.info('Deserializing TensorRT engine...')

    TRT_LOGGER = trt.Logger(trt.Logger.WARNING)

    with open(model, 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
        engine = runtime.deserialize_cuda_engine(f.read())

    return engine


def create_pytorchModel(model, device, opt):
    logger = logging.getLogger('base')
    logger.info('Creating pytorch model...')

    torch_model = RRDBNet(num_in_ch=3, num_out_ch=3, scale=opt['sr_Scale'])
    loadnet = torch.load(model, map_location=torch.device('cpu'))
    # prefer to use params_ema
    keyname = None
    if 'params_ema' in loadnet:
        keyname = 'params_ema'
    elif 'params' in loadnet:
        keyname = 'params'
    if keyname:
        torch_model.load_state_dict(loadnet[keyname], strict=True)
    else:
        torch_model.load_state_dict(loadnet, strict=True)

    torch_model.eval()
    torch_model = torch_model.to(device, memory_format=torch.channels_last)

    return torch_model


def run_model(model, opt, input_tile):
    # <- wand Image float32, RGB
    inputData = np.array(input_tile.export_pixels(channel_map='RGB', storage='float'),
                         dtype=np.float32)\
        .reshape(input_tile.height, input_tile.width, 3)
    # -> Numpy float32, HWC, RGB

    #if inputData.ndim == 2: # unsqueeze one-channel images
    #    inputData = np.expand_dims(inputData, axis=2)
    #if inputData.shape[2] > 3:
    #    inputData = inputData[:, :, :3] # strip alpha channel

    inputData = np.ascontiguousarray( np.transpose(inputData, (2, 0, 1)) )  # HWC ->  CHW
    inputData = np.expand_dims(inputData, 0)                                # CWH -> NCHW

    inferenceRunner = InferenceRunner(model, opt)
    modelOutput = inferenceRunner.run(inputData)                            # run inference
    # -> Numpy float16, NCHW, RGB

    output = modelOutput.squeeze()                                          # NCHW -> CHW
    output = np.clip(output, 0, 1)                                          # clamp to [0, 1]
    output = np.ascontiguousarray( np.transpose(output, (1, 2, 0)) )        # CHW  -> HWC
    output = output.astype('float32')                                       # float16 -> float32

    # <- Numpy float32, HWC, RGB
    modified_tile = wImage.from_array(output, storage='float', channel_map='RGB')
    # -> wand Image float32, RGB

    return modified_tile


def get_model_type(modelFileName):
    modelType = 'N/A'
    if '.trt' in modelFileName:
        modelType = 'tensorrt'
    elif '.onnx' in modelFileName:
        modelType = 'onnx'
    elif '.pth' in modelFileName:
        modelType = 'pytorch'
    else:
        raise ValueError(
            "Unknown model type while using opt['model_type'] == 'auto': "
            '"{}"'.format(modelFileName) )
    return modelType

######################################################
