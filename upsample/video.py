import logging
import subprocess
from  os import remove
import os.path  as osp
import math
from re import sub
import ffmpeg
import numpy

from upsample   import  util   as upUtil


FPS_INVALID_THRESHOLD = 144
USE_FASTER_THAN_SLOWER_X265 = True

fps_info        = 'r_frame_rate'
avg_fps_info    = 'avg_frame_rate'
duration_info   = 'duration'
w_info          = 'width'
h_info          = 'height'
depth_info      = 'bits_per_raw_sample'
colorspace_info = 'color_space'
colortrc_info   = 'color_transfer'
colorprim_info  = 'color_primaries'
colorrange_info = 'color_range'
sar_info        = 'sample_aspect_ratio'
dar_info        = 'display_aspect_ratio'
format_info     = 'pix_fmt'
codec_info      = 'codec_name'
auto            = 'auto'


class VideoClip():
    def __init__(self, start, duration=None, end=None):
        self.start = start
        if duration:
            assert not end
            self.duration = duration
            self.end = start + duration
        else:
            assert end
            self.end = end
            self.duration = end - start

        logger = logging.getLogger('base')
        logger.info('VideoClip(start={}, duration={}, end={})'.format(
            self.start, self.duration, self.end) )

    def __str__(self):
        return '_t[{}-{}]'.format(self.start, self.end)


def duration_to_frameCount(duration, videoInfo):
    if videoInfo['duration']:
        frameCount = math.ceil( (duration / videoInfo['duration']) * videoInfo['frames'] )
    else:
        frameCount = videoInfo['frames']
    return frameCount


def hasAudio(in_videoFileName):
    cmd = ['ffprobe',
           '-hide_banner',
           '-loglevel', '24',
           '-of', 'compact=nk=0:p=0',
           '-select_streams', 'a',
           '-show_entries', 'stream=codec_type',
           '{:s}'.format(in_videoFileName)
           ]
    result = subprocess.run(cmd,
                            universal_newlines=True,
                            stdout=subprocess.PIPE,
                            check=True)
    audioFound = len(result.stdout.split()) > 0
    return audioFound


def get_audio_info(in_videoFileName):
    audioInfo = {}
    audioInfo['hasAudio'] = hasAudio(in_videoFileName)

    if audioInfo['hasAudio']:
        cmd = [
            'ffprobe',
            '-hide_banner',
            '-loglevel', '24',
            '-of', 'compact=nk=0:p=0',
            '-select_streams', 'a',
            '-show_entries', 'stream={}'.format(codec_info),
            '{:s}'.format(in_videoFileName) ]
        result = subprocess.run(cmd,
                                universal_newlines=True,
                                stdout=subprocess.PIPE,
                                check=True)
        last_line = result.stdout.split()[-1]

        for info in last_line.split('|'):
            key, value = info.split('=')
            if key == codec_info:
                audioInfo['codec'] = value.lower()

    return audioInfo


def update_videoInfoFPS(videoInfo):
    videoInfo['fps_float'] = eval(videoInfo['fps'])


def deduceDepthFromFormat(videoInfo):
    # get (max) depth information from video format
    logger = logging.getLogger('base')
    if 'format' in videoInfo:
        logger.info('Deducing depth from video format.')
        vFormat = videoInfo['format']
        if   'p10' in vFormat:
            value = 10
        elif 'p12' in vFormat:
            value = 12
        elif 'p14' in vFormat:
            value = 14
        elif 'p16' in vFormat \
             or '48' in vFormat \
             or '64' in vFormat \
             or 'gray16' in vFormat:
            value = 16
        elif 'f32' in vFormat:
            value = 32
        else:
            value = 8
        videoInfo['depth'] = int(value)
    else:
        logger.info('Using default video depth (8).')
        videoInfo['depth'] = 8


def evaluate_aspect_ratio(videoInfo):
    logger = logging.getLogger('base')

    videoInfo['landscape'] = videoInfo['aspect_ratio'] >= 1.0
    videoInfo['portrait'] = not videoInfo['landscape']
    if videoInfo['landscape']:
        videoInfo['shortSide'] = videoInfo['height']
        videoInfo['longSide']  = videoInfo['width']
        videoInfo['widescreen'] = videoInfo['aspect_ratio'] > (4/3 + 0.01)
    else:
        videoInfo['shortSide'] = videoInfo['width']
        videoInfo['longSide']  = videoInfo['height']
        videoInfo['widescreen'] = videoInfo['aspect_ratio'] < (3/4 - 0.01)
    if videoInfo['shortSide'] > videoInfo['longSide']:
        logger.warning('shortSide > longSide: Calculations might be wrong.')
        # Unsure about this...
        #videoInfo['shortSide'], videoInfo['longSide'] = videoInfo['longSide'],
        #    videoInfo['shortSide']


def get_video_info(filename):
    logger = logging.getLogger('base')
    videoInfo = {}
    videoInfo['filename'] = filename
    videoInfo['ext'] = upUtil.get_fileExtension(filename)

    audioInfo = get_audio_info(filename)
    videoInfo['hasAudio'] = audioInfo['hasAudio']
    if videoInfo['hasAudio']:
        videoInfo['isAudioMp4Compatible'] = True  # default
        videoInfo['isAudioMkvCompatible'] = True  # default
        aCodec = audioInfo['codec']
        if       'wma' in aCodec \
            or 'adpcm' in aCodec \
            or  aCodec in [
                'flac', 'libspeex', 'pcm_alaw', 'pcm_mulaw', 'pcm_u8', 'wmav2', 'pcm_s16le',
                'amr_nb' ]:
            videoInfo['isAudioMp4Compatible'] = False
        if      'amr' in aCodec \
            or aCodec in [
                'cook', 'mlp', 'als', 'dvaudio', 'wmav2' ]:
            videoInfo['isAudioMkvCompatible'] = False

    # TODO consolidate if possible
    frames_cmd   = 'mediainfo --Output="Video;%FrameCount%" "{:s}"'.format(filename)
    rotation_cmd = 'mediainfo --Output="Video;%Rotation%" "{:s}"'.format(filename)

    # Number of frames
    result = subprocess.run(
        frames_cmd,
        shell=True,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        check=True)
    lines = result.stdout.split()
    if len(lines) > 0:
        videoInfo['frames'] = int(lines[-1])
    else:
        # assume one frame
        videoInfo['frames'] = 1

    # ffprobe video
    cmd = ['ffprobe',
           '-hide_banner',
           '-loglevel', '24',
           '-of', 'compact=nk=0:p=0',
           '-select_streams', 'v',
           '-show_entries', 'stream={},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(
               fps_info, avg_fps_info, duration_info, w_info, h_info, depth_info,
               colorspace_info, colortrc_info, colorprim_info,
               colorrange_info, sar_info, dar_info, format_info, codec_info ),
           '{:s}'.format(filename)
           ]
    result = subprocess.run(cmd,
                            universal_newlines=True,
                            stdout=subprocess.PIPE,
                            check=True)
    last_line = result.stdout.split()[-1]
    logger.debug("ffprobe: {}".format(result.stdout))
    logger.debug("last_line: \"{}\"".format(last_line))

    for info in last_line.split('|'):
        logger.debug("\"{}\"".format(info) )

        if info != "":
            key, value = info.split('=')
            if key == fps_info:
                videoInfo['fps'] = str(value)
                update_videoInfoFPS(videoInfo)
            elif key == avg_fps_info:
                videoInfo['fps_avg'] = str(value)
            elif key == duration_info:
                if value != 'N/A':
                    videoInfo['duration'] = float(value)
                else:
                    videoInfo['duration'] = None
                    logger.warning('Unknown video duration.')
            elif key == w_info:
                videoInfo['width'] = int(value)
            elif key == h_info:
                videoInfo['height'] = int(value)
            elif key == format_info:
                videoInfo['format'] = str(value)
            elif key == depth_info:
                doDeduceDepthFromFormat = False
                if value == 'N/A':
                    doDeduceDepthFromFormat = True
                else:
                    videoInfo['depth'] = int(value)
            elif key == colorspace_info:
                videoInfo['colorspace'] = str(value)
            elif key == colortrc_info:
                videoInfo['colortrc'] = str(value)
            elif key == colorprim_info:
                videoInfo['colorprim'] = str(value)
            elif key == colorrange_info:
                videoInfo['colorrange'] = str(value)
                videoInfo['limitedRange'] = videoInfo['colorrange'] not in ['full', 'pc', 'jpeg']
            elif key == sar_info:
                videoInfo['SAR'] = str(value)
            elif key == dar_info:
                videoInfo['DAR'] = str(value)
            elif key == codec_info:
                videoInfo['codec'] = str(value)

    if doDeduceDepthFromFormat:
        deduceDepthFromFormat(videoInfo)

    # ensure valid fps value
    useDefaultFPS = False
    if videoInfo['fps_float'] > FPS_INVALID_THRESHOLD:
        logger.warning( "FPS value ({} -> {}) is too high and probably invalid.".format(
            videoInfo["fps"], eval(videoInfo["fps"]) ) )
        if (videoInfo['fps_avg'] != ""):
            logger.info(videoInfo['fps_avg'])
            if int(videoInfo['fps_avg'].split('/')[1]) > 0:
                valid = eval(videoInfo['fps_avg']) > 0 and eval(videoInfo['fps_avg']) <= 1000
                if valid:
                    logger.info( "Using average FPS value ({},{}).".format(
                        videoInfo["fps_avg"], eval(videoInfo["fps_avg"])) )
                    videoInfo["fps"] = videoInfo["fps_avg"]
                else:
                    useDefaultFPS = True
            else:
                useDefaultFPS = True
        else:
            useDefaultFPS = True
        if useDefaultFPS:
            logger.warning("Using default FPS value (24).")
            videoInfo["fps"] = "24"
        update_videoInfoFPS(videoInfo)

    result = subprocess.run(
        rotation_cmd,
        shell=True,
        universal_newlines=True,
        stdout=subprocess.PIPE,
        check=True)
    lines = result.stdout.split()
    if len(lines) > 0:
        videoInfo['rotation'] = int(float(lines[-1]))

    dar = videoInfo['DAR']      # Display Aspect Ratio
    if dar != 'N/A':
        aspect_ratio = eval( dar.replace(':', '/') )
    else:
        aspect_ratio = videoInfo['width']/videoInfo['height']
        sar = videoInfo['SAR']  # Sample Aspect Ratio (i.e. Pixel Aspect Ratio)
        if sar != 'N/A':
            aspect_ratio /= eval( sar.replace(':', '/') )

    if 'rotation' in videoInfo:
        rotation = videoInfo['rotation']
        absRotation = abs(rotation)
        if absRotation == 90 or absRotation == 270:
            aspect_ratio = 1 / aspect_ratio
            tmp = videoInfo['height']
            videoInfo['height'] = videoInfo['width']
            videoInfo['width']  = tmp
        elif absRotation % 90 != 0:
            raise NotImplementedError('Rotation of {}° not implemented.'.format(rotation))

    videoInfo['aspect_ratio'] = aspect_ratio
    assert aspect_ratio > 0, videoInfo

    evaluate_aspect_ratio(videoInfo)

    logger.info(videoInfo)
    return videoInfo


def get_video_out_dir(videoInfo):
    logger = logging.getLogger('base')

    path_root = osp.split( videoInfo['filename'] )[0]
    path_root = osp.normpath(path_root)

    logger.debug( {"path_root": path_root} )

    return path_root


def sws2zscale_matrix(swsColorspace):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':     '709',         # BT.709
        'fcc':       'input',       # FCC        (no zscale equivalent)
        'bt470bg':   '470bg',       # BT.470BG or BT.601-6 625,     i.e. "PAL"
        'smpte170m': '170m',        # SMPTE-170M or BT.601-6 525,   i.e. "NTSC"
        'smpte240m': 'input',       # SMPTE-240M (no zscale equivalent)
        'ycgco':     'input',       # YCgCo      (no zscale equivalent)
        'bt2020ncl': '2020_ncl',    # BT.2020 with non-constant luminance
        'bt2020':    '2020_cl',     # (not in sws colorspace, but in sws colormatrix)
    }

    zscaleMatrix = switcher.get(swsColorspace, 'input')
    if zscaleMatrix == 'input':
        logger.warning( 'Colorspace "{}" not recognized.'.format(swsColorspace) )
    else:
        logger.info("Color: colorspace {} ->  matrix {}".format(swsColorspace, zscaleMatrix) )
    return zscaleMatrix


def zscale2sws_matrix(zscaleMatrix):
    logger = logging.getLogger('base')
    switcher = {
        '709':       'bt709',       # BT.709
        '470bg':     'bt470bg',     # BT.470BG or BT.601-6 625,     i.e. "PAL"
        '170m':      'smpte170m',   # SMPTE-170M or BT.601-6 525,   i.e. "NTSC"
        '2020_ncl':  'bt2020ncl',   # BT.2020 with non-constant luminance
        '2020_cl':   'bt2020',      # (not in sws colorspace, but in sws colormatrix)
    }

    swsColorspace = switcher.get(zscaleMatrix, 'input')
    if swsColorspace == 'input':
        logger.warning( 'Colorspace "{}" not recognized. Using bt709.'.format(zscaleMatrix) )
        swsColorspace = 'bt709'
    else:
        logger.info("Color: matrix {} ->  colorspace {}".format(zscaleMatrix, swsColorspace) )
    return swsColorspace


def zscale_matrix2primaries(zscaleMatrix):
    logger = logging.getLogger('base')
    switcher = {
        '709':      '709',
        '470bg':    '170m',
        '170m':     '170m',
        '2020_ncl': '2020',
        '2020_cl':  '2020',
    }

    zscalePrimaries = switcher.get(zscaleMatrix, 'input')
    if zscalePrimaries == 'input':
        logger.warning( 'Color matrix "{}" not recognized.'.format(zscaleMatrix) )
    else:
        logger.info('matrix({}) --> primaries({})'.format(zscaleMatrix, zscalePrimaries))
    return zscalePrimaries


def sws_space2primaries(swsSpace):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':        'bt709',
        'fcc':          'unknown',
        'bt470bg':      'bt470bg',
        'smpte170m':    'smpte170m',
        'smpte240m':    'smpte240m',
        'ycgco':        'unknown',
        'bt2020ncl':    'bt2020',
    }

    swsPrimaries = switcher.get(swsSpace, 'unknown')
    if swsPrimaries == 'unknown':
        logger.warning( 'Color matrix "{}" not recognized or conversion not possible.'.format(swsSpace) )
        swsPrimaries = 'bt709'
    logger.info('colorspace({}) --> primaries({})'.format(swsSpace, swsPrimaries))
    return swsPrimaries


def zscale_matrix2transfer(zscaleMatrix, videoInfo):
    logger = logging.getLogger('base')
    switcher = {
        '709':      '709',
        '470bg':    '601',
        '170m':     '601',
        '2020_ncl': '2020_',
        '2020_cl':  '2020_',
    }

    zscaleTransfer = switcher.get(zscaleMatrix, 'input')
    if zscaleTransfer == '2020_':
        depth = videoInfo['depth']
        if   depth > 10:
            zscaleTransfer += '12'
        elif depth > 8:
            zscaleTransfer += '10'
        else:
            raise NotImplementedError('Depth {:d} not usable with BT.2020 primaries'.format(depth))

    if zscaleTransfer == 'input':
        logger.warning( 'Color matrix "{}" not recognized.'.format(zscaleMatrix) )
    else:
        logger.info('matrix({}) --> transfer({})'.format(zscaleMatrix, zscaleTransfer))
    return zscaleTransfer


def sws_space2transfer(swsSpace, videoInfo):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':        'bt709',
        'fcc':          'unknown',
        'bt470bg':      'bt709',  #'bt470bg',
        'smpte170m':    'smpte170m',
        'smpte240m':    'smpte240m',
        'ycgco':        'unknown',
        'bt2020ncl':    'bt2020-',
    }

    swsTrc = switcher.get(swsSpace, 'unknown')
    if swsTrc == 'bt2020-':
        depth = videoInfo['depth']
        if   depth > 10:
            swsTrc += '12'
        elif depth > 8:
            swsTrc += '10'
        else:
            raise NotImplementedError('Depth {:d} not usable with BT.2020 primaries'.format(depth))

    if swsTrc == 'unknown':
        logger.warning( 'Color matrix "{}" not recognized.'.format(swsSpace) )
        swsTrc = 'bt709'
    logger.info('colorspace({}) --> transfer({})'.format(swsSpace, swsTrc))
    return swsTrc


def sws2zscale_primaries(swsPrimaries):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':     '709',     # BT.709,                       i.e. "HD"
        'bt470m':    '170m',    # BT.470M
        'bt470bg':   '170m',    # BT.470BG or BT.601-6 625,     i.e. "PAL"
        'smpte170m': '170m',    # SMPTE-170M or BT.601-6 525,   i.e. "NTSC"
        'smpte240m': '240m',    # SMPTE-240M
        'film':      'input',   #                             (no zscale equivalent)
        'smpte431':  'input',   # SMPTE-431                   (no zscale equivalent)
        'smpte432':  'input',   # SMPTE-432                   (no zscale equivalent)
        'bt2020':    '2020',    # BT.2020,                      i.e. "UHD", "4k"
        'jedec-p22': 'input',   # JEDEC P22, EBU Tech. 3213-E (no zscale equivalent)
    }

    zscalePrimaries = switcher.get(swsPrimaries, 'input')
    if zscalePrimaries == 'input':
        logger.warning( 'Primaries "{}" not recognized.'.format(swsPrimaries) )
    else:
        logger.info("Color: primaries {} -> {}".format(swsPrimaries, zscalePrimaries) )
    return zscalePrimaries


def zscale2sws_primaries(zscalePrimaries):
    logger = logging.getLogger('base')
    switcher = {
        '709':     'bt709',     # BT.709,                       i.e. "HD"
        '170m':    'bt170m',    # BT.470M
        '240m':    'bt240m',    # SMPTE-240M
        '2020':    'bt2020',    # BT.2020,                      i.e. "UHD", "4k"
    }

    swsPrimaries = switcher.get(zscalePrimaries, 'input')
    if swsPrimaries == 'input':
        logger.warning( 'Primaries "{}" not recognized. Using bt709.'.format(zscalePrimaries) )
        swsPrimaries = 'bt709'
    else:
        logger.info("Color: primaries {} -> {}".format(zscalePrimaries, swsPrimaries) )
    return swsPrimaries


def zscale_primaries2matrix(zscalePrimaries, videoInfo):
    logger = logging.getLogger('base')
    switcher = {
        '709':  '709',
        '170m': '601',
        '240m': 'input',
        '2020': '2020_cl',  # Could also be '2020_ncl', but if unknown, assume constant luminance.
    }

    zscaleMatrix = switcher.get(zscalePrimaries, 'input')
    if zscaleMatrix == 'input':
        logger.warning( 'Color primaries "{}" not recognized.'.format(zscalePrimaries) )
    else:
        if zscaleMatrix == '601':
            zscaleMatrix = '470bg'
        elif videoInfo['colorprim'] == 'bt470m':
            zscaleMatrix = '470m'
        elif videoInfo['colorprim'] == 'smpte170m':
            zscaleMatrix = '170m'
        else:
            logger.error( "Color: zscale_primaries2matrix unknown sws primaries ({})".format(
                videoInfo['colorprim']) )
            zscaleMatrix = '170m'
        logger.info('Color: primaries {} -> matrix {}'.format(zscalePrimaries, zscaleMatrix))
    return zscaleMatrix


def sws_primaries2space(swsPrimaries):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':  'bt709',
        'bt470m': 'smpte170m',
        'bt470bg': 'bt470bg',
        'smpte170m': 'smpte170m',
        'smpte240m': 'smpte240m',
        'film': 'unknown',
        'smpte431': 'unknown',
        'smpte432': 'unknown',
        'bt2020': 'bt2020ncl',
        'jedec-p22': 'unknown'
    }

    swsSpace = switcher.get(swsPrimaries, 'unknown')
    if swsSpace == 'unknown':
        logger.warning( 'Color primaries "{}" not recognized or conversion not possible.'.format(swsPrimaries) )
        swsSpace = 'bt709'
    logger.info('Color: primaries {} -> space {}'.format(swsPrimaries, swsSpace))
    return swsSpace


def sws2zscale_transfer(swsTrc):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':        '709',            # BT.709
        'bt470m':       '601',            # BT.470M
        'bt470bg':      '601',            # BT.470BG
        'gamma22':      'input',          # Constant gamma of 2.2 (no zscale equivalent)
        'gamma28':      'input',          # Constant gamma of 2.8 (no zscale equivalent)
        'smpte170m':    '601',            # SMPTE-170M, BT.601-6 625 or BT.601-6 525
        'smpte240m':    'input',          # SMPTE-240M            (no zscale equivalent)
        'srgb':         'iec61966-2-1',   # sRGB
        'iec61966-2-1': 'iec61966-2-1',   # sRGB
        'iec61966-2-4': 'input',          # xvYCC                 (no zscale equivalent)
        'xvycc':        'input',          # xvYCC                 (no zscale equivalent)
        'bt2020-10':    '2020_10',        # BT.2020 for 10-bits content
        'bt2020-12':    '2020_12',        # BT.2020 for 12-bits content
        # zscale inputs w/o sws equivalent are 'smpte2084' (PQ) and 'arib-std-b67' (HLG)
    }

    zscaleTransfer = switcher.get(swsTrc, 'input')
    if zscaleTransfer == 'input':
        logger.warning( 'Transfer function "{}" not recognized.'.format(swsTrc) )
    else:
        logger.info("Color: trc {} -> {}".format(swsTrc, zscaleTransfer) )
    return zscaleTransfer


def zscale2sws_transfer(zscaleTransfer):
    logger = logging.getLogger('base')
    switcher = {
        '709':          'bt709',            # BT.709
        '601':          'smpte170m',        # BT.470M or BT.470BG or SMPTE-170M
        'iec61966-2-1': 'srgb',             # sRGB
        'bt2020_10':    'bt2020-10',        # BT.2020 for 10-bits content
        'bt2020_12':    'bt2020-12',        # BT.2020 for 12-bits content
    }

    swsTrc = switcher.get(zscaleTransfer, 'input')
    if swsTrc == 'input':
        logger.warning( 'Transfer function "{}" not recognized. Using bt709.'.format(zscaleTransfer) )
    else:
        logger.info("Color: trc {} -> {}".format(zscaleTransfer, swsTrc) )
    return swsTrc


def zscale_transfer2matrix(zscaleTransfer):
    logger = logging.getLogger('base')
    switcher = {
        '709':          '709',
        '170m':         '601',
        'iec61966-2-1': '709',
        '2020_10':      '2020_cl',  # Could also be '2020_ncl'. If unknown, assume const luminance.
        '2020_12':      '2020_cl',  # Could also be '2020_ncl'. If unknown, assume const luminance.
    }

    zscaleMatrix = switcher.get(zscaleTransfer, 'input')
    if zscaleMatrix == 'input':
        logger.warning( 'Transfer function "{}" not recognized.'.format(zscaleTransfer) )
    else:
        logger.info('transfer({}) --> matrix({})'.format(zscaleTransfer, zscaleMatrix))
    return zscaleMatrix


def sws_transfer2space(swsTrc):
    logger = logging.getLogger('base')
    switcher = {
        'bt709':        'bt709',
        'bt470m':       'smpte170m',
        'bt470bg':      'bt470bg',
        'gamma22':      'unknown',
        'gamma28':      'unknown',
        'smpte170m':    'smpte170m',
        'smpte240m':    'smpte240m',
        'srgb':         'bt709',
        'iec61966-2-1': 'bt709',
        'iec61966-2-4': 'unknown',
        'xvycc':        'unknown',
        'bt2020-10':    'bt2020ncl',
        'bt2020-12':    'bt2020ncl',
    }

    swsSpace = switcher.get(swsTrc, 'unknown')
    if swsSpace == 'unknown':
        logger.warning( 'Transfer function "{}" not recognized or conversion not possible.'.format(swsTrc) )
        swsSpace = 'bt709'
    logger.info('transfer({}) --> matrix({})'.format(swsTrc, swsSpace))
    return swsSpace


def detect_PAL_NTSC(videoInfo):
    logger = logging.getLogger('base')
    ## guesswork
    shortSide = videoInfo['shortSide']
    longSide  = videoInfo['longSide']
    fps       = round(videoInfo['fps_float'])
    # TODO encoders might encode SD videos as bt.601, should we assume that? mpv seems to do that.
    assume_smallVid_is_bt601 = False
    # Widescreen SD is probably from a professional source. Should be rare enough.
    isSD = longSide <= 720 and (not videoInfo['widescreen'] or shortSide in [576, 480] or assume_smallVid_is_bt601)
    PAL_override  = isSD and \
        ( shortSide == 576 or upUtil.floatEqual(fps, 25) or upUtil.floatEqual(fps, 50) or '50' in videoInfo['fps'] )
    NTSC_override = isSD and not PAL_override
    logger.info('detect_PAL_NTSC: isSD={}, PAL_override={}, NTSC_override={}'.format(
        upUtil.stringToBool(isSD), upUtil.stringToBool(PAL_override),
        upUtil.stringToBool(NTSC_override)) )
    return PAL_override, NTSC_override


def do_bt601_quirk(videoInfo):
    byCodec = videoInfo['codec'] in ['vp8', 'mjpeg']
    return byCodec


def get_color_info(videoInfo, zscale=True):
    logger = logging.getLogger('base')

    if zscale:
        matrix = primaries = transfer = 'input'

        if videoInfo['colorprim'] != 'unknown':
            primaries = sws2zscale_primaries(videoInfo['colorprim'])

        if videoInfo['colortrc'] != 'unknown':
            transfer  = sws2zscale_transfer(videoInfo['colortrc'])

        if videoInfo['colorspace'] != 'unknown':
            matrix    = sws2zscale_matrix(videoInfo['colorspace'])
        else:   # colorspace unknown
            if primaries != 'input':        # guess matrix from primaries
                matrix = zscale_primaries2matrix(primaries, videoInfo)
            elif transfer != 'input':       # guess matrix from transfer function
                matrix = zscale_transfer2matrix(transfer)
            else:   # primaries and transfer unknown
                PAL_override, NTSC_override = detect_PAL_NTSC(videoInfo)
                if PAL_override:
                    matrix    = '470bg'
                    primaries = '170m'
                    transfer  = '601'
                elif NTSC_override:
                    matrix    = '170m'
                    primaries = '170m'
                    transfer  = '601'
                else:   # using BT.709 default
                    if not do_bt601_quirk(videoInfo):
                        matrix = primaries = transfer = '709'
                    else:   # webm/jpeg using bt.601 colormatrix quirk
                        logger.info('Using webm/jpeg colormatrix quirk.')
                        matrix    = '170m'
                        primaries = transfer = '709'


        if primaries == 'input':            # guess primaries from matrix
            primaries = zscale_matrix2primaries(matrix)
        if transfer == 'input':          # guess transfer function from matrix
            transfer = zscale_matrix2transfer(matrix, videoInfo)

        logger.info( 'Color: sws(cs={:s}, p={:s}, t={:s}) --> zscale(m={:s}, p={:s}, t={:s})'.format(
            videoInfo['colorspace'], videoInfo['colorprim'], videoInfo['colortrc'],
            matrix, primaries, transfer
        ) )
        return matrix, primaries, transfer
    else:
        space = primaries = transfer = 'unknown'
        if videoInfo['colorprim'] != 'unknown':
            primaries = videoInfo['colorprim']
        if videoInfo['colortrc'] != 'unknown':
            transfer  = videoInfo['colortrc']
        if videoInfo['colorspace'] != 'unknown':
            space    = videoInfo['colorspace']
        else:   # colorspace unknown
            if primaries != 'unknown':        # guess space from primaries
                space = sws_primaries2space(primaries)
            elif transfer != 'unknown':       # guess space from transfer function
                space = sws_transfer2space(transfer)
            else:   # primaries and transfer unknown
                PAL_override, NTSC_override = detect_PAL_NTSC(videoInfo)
                if PAL_override:
                    space = primaries = 'bt470bg'
                    transfer = 'bt709'  # TODO investigate: bt470bg transfer makes output too dark
                elif NTSC_override:
                    space = primaries = 'smpte170m'
                    transfer = 'bt709'  # TODO investigate: smpte170m transfer makes output too dark
                else:   # using BT.709 default
                    if not do_bt601_quirk(videoInfo):
                        space = primaries = transfer = 'bt709'
                    else:
                        space = 'smpte170m'
                        primaries = transfer = 'bt709'

        if primaries == 'unknown':            # guess primaries from space
            primaries = sws_space2primaries(space)
        if transfer == 'unknown':          # guess transfer function from space
            transfer = sws_space2transfer(space, videoInfo)

        logger.info( 'Color: sws(cs={:s}, p={:s}, t={:s}) --> sws(cs={:s}, p={:s}, t={:s})'.format(
            videoInfo['colorspace'], videoInfo['colorprim'], videoInfo['colortrc'],
            space, primaries, transfer
        ) )

        return space, primaries, transfer


def get_extracted_colorspace(videoInfo, opt, useZScale=False):    # FIXME function name
    logger = logging.getLogger('base')
    colorspace_override = opt['in_ColorspaceOverride']
    doExtractFullRange  = opt['in_extractFullRange']
    pre_filter = ''
    colorConversionFilter = ''

    # color correction
    colorspace = videoInfo['colorspace']
    if colorspace_override != '':
        colorspace = colorspace_override
    logger.info('colorspace={}'.format(colorspace))

    # color range correction
    if videoInfo['limitedRange']:
        in_range = 'tv'
        if doExtractFullRange:
            out_range = 'pc'
        else:
            out_range = 'tv'
    else:
        in_range = 'pc'
        out_range = 'pc'

    in_matrix, in_primaries, in_transfer = get_color_info(videoInfo, zscale=useZScale)
    if useZScale:    # TODO set out_colorspace
        if not (in_matrix == in_primaries == in_transfer == '709'):
            in_color = 'min={:s}:pin={:s}:tin={:s}'.format(in_matrix, in_primaries, in_transfer)

            if colorspace in ['bt2020', 'bt2020ncl', 'bt2020-10', 'bt2020-12']:
                out_color  = 'm=2020_ncl:p=2020'
                out_transfer    = '2020_{}'.format(videoInfo['depth'])
                #in_matrix, in_primaries, in_transfer = get_color_info(videoInfo)
            elif colorspace == 'smpte240m':     # "old-school" UHD
                raise NotImplementedError( 'SMPTE240m input is not implemented.' )
            else:
                out_color  = 'm=709:p=709'
                out_transfer    = '709'

            out_color += ':t={}'.format(out_transfer)

            # color range correction
            if videoInfo['limitedRange']:
                in_color  += ':rin=limited'
                pre_filter = 'setrange=limited, '
                if doExtractFullRange:
                    out_color += ':r=full, setrange=full'
                else:
                    out_color += ':r=limited, setrange=limited'
            else:
                in_color += ':rin=full'
                pre_filter = 'setrange=full, '
                out_color += ':r=full, setrange=full'

            colorConversionFilter = ', zscale=d=none:{:s}:{:s}'.format(in_color, out_color)
    else:
        if not (in_matrix == in_primaries == in_transfer == 'bt709'):
            in_color = 'ispace={:s}:iprimaries={:s}:itrc={:s}'.format(in_matrix, in_primaries,
                                                                      in_transfer)
            if colorspace in ['bt2020', 'bt2020ncl', 'bt2020-10', 'bt2020-12']:
                out_matrix = 'bt2020ncl'
                out_primaries = 'bt2020'
                out_color  = 'space=bt2020ncl:primaries=bt2020'
                out_transfer = 'bt2020-{}'.format(videoInfo['depth'])
            elif colorspace == 'smpte240m':     # "old-school" UHD
                raise NotImplementedError( 'SMPTE240m input is not implemented.' )
            else:
                out_matrix = 'bt709'
                out_primaries = 'bt709'
                out_transfer = 'bt709'

            out_color = 'space={}:primaries={}:trc={}'.format(out_matrix, out_primaries,
                                                              out_transfer)

            # crop to even length
            cropwidth = videoInfo['width']
            cropheight = videoInfo['height']
            if cropwidth % 2 > 0:
                cropwidth -= 1
            if cropheight % 2 > 0:
                cropheight -= 1
            if cropwidth != videoInfo['width'] or cropheight != videoInfo['height']:
                pre_filter += 'crop=w={}:h={}:x=0:y=0:exact=1, '.format(cropwidth, cropheight)

            # color range correction
            out_color += ':irange={}:range={}'.format(in_range, out_range)

            colorConversionFilter = ', colorspace=dither=none:{:s}:{:s}'.format(in_color,
                                                                                out_color)

    if opt['out_noColorConversion']:
        in_colorspace = {'matrix': in_matrix, 'primaries': in_primaries,
                         'transfer': in_transfer, 'range': out_range}
    else:   # use converted color space
        in_colorspace = {'matrix': out_matrix, 'primaries': out_primaries,
                         'transfer': out_transfer, 'range': out_range}

    return in_colorspace, pre_filter, colorConversionFilter     # FIXME refactor


def extract_frames(filename, videoInfo, workingDir, opt, clip=None):
    """ Extract (all) video frames as padded n-bit linear RGB (TIFF) files """
    logger = logging.getLogger('base')

    fps                 = opt['out_fps']
    max_frames          = opt['out_maxFrames']

    logger.debug('extractFrames(filename={:s}, workingDir={:s}, fps={:f}, max_frames={:d})'.format(
        filename, workingDir, fps, max_frames
    ) )

    outputname = osp.join(workingDir,
                          '%{}{}'.format(upUtil.FRAME_INDEX_STR, opt['output_ext']))

    # frame selection
    fps_str = ''
    if fps > 0:
        fps_str = ', fps=fps={:f}:eof_action=pass'.format(fps)

    useZScale = False
    in_colorspace, pre_filter, colorConversionFilter = get_extracted_colorspace(videoInfo,
                                                                                opt, useZScale)

    color_str = ''
    if not opt['out_noColorConversion']:
        color_str = colorConversionFilter

    color_str += ', setparams=colorspace={}:color_primaries={}:color_trc={}:range={}'.format(
        in_colorspace['matrix'], in_colorspace['primaries'],
        in_colorspace['transfer'], in_colorspace['range'] )

    filterStr = pre_filter + 'format=pix_fmts=yuv444p16' + fps_str + color_str

    maxDepth = max(opt['in_extractBitDepth'], videoInfo['depth')
    if (maxDepth > 8):
        if maxDepth > 16:
            raise NotImplementedError('Bitdepths above 16 not supported by ffmpeg image codecs.')
        else:
            out_format = 'rgb48'
    else:
        out_format = 'rgb24'
    if videoInfo['depth' > opt['in_extractBitDepth']:
        if 'd=none' in filterStr:
            filterStr.replace('d=none', 'd=error_diffusion')
        elif 'dither=none' in filterStr:
            filterStr.replace('dither=none', 'dither=fsb')

    filterOptions = []
    if filterStr != '':
        filterOptions = ['-vf', filterStr]

    # max frames
    if isinstance(max_frames, int) and max_frames > 0:
        filterOptions = [*filterOptions, '-frames', str(max_frames)]

    timeIndices = []
    if clip:
        timeIndices = [
            '-ss', str(clip.start),
            '-to', str(clip.end) ]
    logger.debug(timeIndices)

    cmd = [
        'ffmpeg', '-hide_banner',
        '-i', filename,
        #'-v', 'verbose',
        *timeIndices,
        '-an', '-sn', '-dn',
        *filterOptions,
        '-pix_fmt', out_format,
        outputname ]

    logger.info(cmd)
    subprocess.run(cmd, check=True)

    return in_colorspace


def get_audio_fileName():
    ext = '.mka'
    return 'audio' + ext


def extract_audio(videoInfo, opt, audioDir=''):
    logger = logging.getLogger('base')
    videoFilePath = videoInfo['filename']

    audioDir = opt['extractDir'] if audioDir == '' else audioDir

    audioFilePath = osp.join( audioDir, get_audio_fileName() )

    codec = get_audio_codec_options(videoInfo, opt['out_videoExt'], False)
    if 'copy' not in codec:
        # Audio will be converted to compatible codec
        videoInfo['isAudioMp4Compatible'] = True
        videoInfo['isAudioMkvCompatible'] = True

    cmd = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-i', videoFilePath,
        '-vn', '-sn', '-dn',
        *codec,
        audioFilePath ]
    logger.info(cmd)
    subprocess.run(cmd, check=True)

    return audioFilePath


def get_video_codec_options(doLosslessEncoding, videoInfo=None, chromaSubType='444', scale=1.0,
                            encoder=auto, codec=auto, anime=False):
    logger = logging.getLogger('base')
    codecOptions = ['-c:v']

    if doLosslessEncoding:
        if encoder == auto:
            encoder = 'nvenc'

        if codec == 'hevc' or codec == auto:
            if encoder == 'nvenc':
                codecOptions = [
                    *codecOptions,
                    'hevc_nvenc',
                    '-preset', 'p7',
                    '-tune', 'lossless',
                    '-tier', 'high',
                    '-rc', 'constqp',
                    '-qp', '0' ]
            elif encoder == 'x265':
                codecOptions = [
                    *codecOptions,
                    'libx265',
                    '-preset', 'slow',
                    '-x265-params', 'lossless=1' ]
            else:
                raise NotImplementedError('Lossless encoder "{}" not implemented.'.format(encoder))
        else:
            raise NotImplementedError( 'Lossless codec "{}" not implemented.'.format(codec) )
    else:
        scaled_w = videoInfo['width']  * scale
        scaled_h = videoInfo['height'] * scale
        bitRate_ratio = 0.303819444444  # 7000/(math.sqrt(1280*720)*24), assuming 420 chroma
        # bitRate in kB:
        bitRate = round(bitRate_ratio * math.sqrt(scaled_w * scaled_h) * videoInfo['fps_float'])
        if chromaSubType == '444':
            bitRate *= 3/2
        elif chromaSubType == '422':
            bitRate *= 3/4
        logger.debug({'bitrate': bitRate})

        if codec == auto:
            #if videoInfo['shortSide'] > 2160:
            #    codec = 'av1'
            if videoInfo['shortSide'] >= 600:
                codec = 'hevc'
            else:
                codec = 'avc'

        if codec == 'av1':
            if encoder == auto:
                encoder = 'aom'

            if encoder == 'svt':
                codecOptions = [
                    *codecOptions,
                    'libsvtav1',
                    'preset', 3,
                    '-b:v', '{}k'.format(bitRate),
                    '-qp', '12' ]
            elif encoder == 'aom':
                logger.warning(' libaom-av1 not recommended for now (April 2021)' )
                codecOptions = [
                    *codecOptions,
                    'libaom-av1',
                    'cpu-used', 4,
                    '-b:v', '{}k'.format(bitRate),
                    '-crf', '12',
                    '-tiles', '6x4',
                    'row-mt', '1' ]
            else:
                raise NotImplementedError( 'AV1 encoder "{}" not implemented.'.format(encoder) )
        elif codec == 'avc':
            if encoder == auto or encoder == 'sw':
                encoder = 'x264'

            if encoder == 'x264':
                # slower  is 6x faster than x265 slow
                # slowest is slower    than x265 slower
                # size    is 2x larger than x265 at the same preset (slower)
                # quality is slightly worse than x265 at the same preset (slower)
                tune = 'film'
                if anime:
                    tune = 'animation'
                crf = 12
                if chromaSubType == '444':
                    crf -= 2
                elif chromaSubType == '422':
                    crf -= 1
                codecOptions = [
                    *codecOptions,
                    'libx264',
                    '-preset', 'slower',
                    '-tune', tune,
                    '-b:v', '{}k'.format(bitRate),
                    '-crf', str(crf) ]
            elif encoder == 'nvenc':
                profile = 'high'
                if chromaSubType == '444' or chromaSubType == '422':
                    profile = 'high444p'
                codecOptions = [
                    *codecOptions,
                    'h264_nvenc',
                    '-preset', 'p7',
                    '-profile:v', profile,
                    '-tune', 'hq',
                    '-rc', 'constqp',
                    '-qp', '4',
                    '-rc-lookahead', '0'
                ]
                pass
            else:
                raise NotImplementedError( 'AVC encoder "{}" not implemented.'.format(encoder) )
        elif codec == 'hevc':
            if encoder == auto or encoder == 'sw':
                encoder = 'x265'

            if encoder == 'x265':
                x265Params = 'psy-rd=2.5:psy-rdoq=4.0:sao=0:deblock=-1,-1'
                if USE_FASTER_THAN_SLOWER_X265:
                    x265Params += ':limit-refs=3:amp=0:rd=4'
                crf = 12
                if chromaSubType == '444':
                    crf -= 2
                    x265Params += ':cbqpoffs=4:crqpoffs=4'  # default: 6
                elif chromaSubType == '422':
                    crf -= 1
                    x265Params += ':cbqpoffs=5:crqpoffs=5'  # default: 6
                if videoInfo['depth'] < 10:
                    x265Params += ':aq-mode=3'
                codecOptions = [
                    *codecOptions,
                    'libx265',
                    '-preset', 'slower',
                    '-b:v', '{}k'.format(bitRate),
                    '-crf', str(crf),
                    '-x265-params', x265Params ]
                if anime:
                    codecOptions.append('-tune')
                    codecOptions.append('animation')
            elif encoder == 'nvenc':    # ~4x bigger than x265, ~87x faster
                profile = 'main'
                if videoInfo['depth'] > 8:
                    profile = 'main10'
                codecOptions = [
                    *codecOptions,
                    'hevc_nvenc',
                    '-preset', 'p7',
                    '-profile:v', profile,
                    '-tune', 'hq',
                    '-tier', 'high',
                    '-rc', 'constqp',
                    '-qp', '4',
                    '-rc-lookahead', '0'
                ]
            else:
                raise NotImplementedError( 'HEVC encoder "{}" not implemented.'.format(encoder) )
        else:
            raise NotImplementedError( 'Lossfull codec "{}" not implemented.'.format(codec) )

    w = h = 'n/a'
    if videoInfo:
        w = videoInfo['width']
        h = videoInfo['height']
    logger.info( 'get_codec_str({}, {}, {}, {}, {}, {}): {})'.format(
        (w, h), doLosslessEncoding, chromaSubType, scale, encoder, codec, codecOptions ) )

    return codecOptions


def get_audio_codec_options(videoInfo, ext, forceAudioReencoding=False):
    logger = logging.getLogger('base')
    audioOptions = []
    conversionOptions = ['-c:a', 'libopus']

    if videoInfo['hasAudio']:
        if (ext == '.mp4' and not videoInfo['isAudioMp4Compatible']) or \
           (ext == '.mkv' and not videoInfo['isAudioMkvCompatible']) or \
           forceAudioReencoding:
            audioOptions = conversionOptions
            logger.warning('Converting audio to Opus codec.')
        else:
            audioOptions = ['-c:a', 'copy']
            logger.debug('Using extracted audio file as-is.')

    return audioOptions


def get_video_output_size(videoInfo, opt):
    logger = logging.getLogger('base')

    sr_scale        = opt['sr_Scale']
    post_sr_scale   = opt['postSRScale']
    neatScaling     = opt['neatScaling']
    landscape       = videoInfo['landscape']
    aspect_ratio    = videoInfo['aspect_ratio']
    sr_w = videoInfo['width']  * sr_scale
    sr_h = videoInfo['height'] * sr_scale
    logger.debug( (sr_w, sr_h) )

    if post_sr_scale > 0:
        current_size = videoInfo['longSide'] * sr_scale
        post_sr_size = current_size * post_sr_scale

        if neatScaling:
            mod = 40    # TODO -> opt
            post_sr_size = mod * (post_sr_size // mod)

        if landscape:
            w = post_sr_size
            h = math.ceil(w / aspect_ratio)
            h -= h % 4
        else:   # portrait
            h = post_sr_size
            w = math.ceil(h * aspect_ratio)
            w -= w % 4

        w = int(w)
        h = int(h)
    else:   # using override_res
        raise NotImplementedError('override_res not implemented.')
        #if landscape:
        #    w = override_res['l']
        #    h = override_res['s']
        #else:
        #    w = override_res['s']
        #    h = override_res['l']

    size = {}
    size['sr_w'] = sr_w
    size['sr_h'] = sr_h
    size['w']    = w
    size['h']    = h
    return size


def get_video_parameters(videoInfo, audio_file, opt, model, suffix, custom_filter, anime,\
                         speedMultiplier=1, zscale=True, fixColor=True, customPath=None):
    logger = logging.getLogger('base')
    parameters = {}

    lossless            = opt['lossless']
    sr_scale            = opt['sr_Scale']
    force_hibit_output  = opt['out_forceHighDepth']
    doChromaSub         = opt['out_doLossfullChromaSubsampling']
    noColorConversion   = opt['out_noColorConversion']
    slowDownGIF         = videoInfo['ext'] == '.gif' and opt['out_slowDownGIF']

    logger.info(videoInfo)
    parameters['aspect_ratio'] = videoInfo['aspect_ratio']
    landscape = videoInfo['landscape']

    size = get_video_output_size(videoInfo, opt)

    doScale = (size['w'] != size['sr_w']) and (size['h'] != size['sr_h'])
    logger.debug(
        "new Width: {}, new Height: {}, doScale={:b}".format(size['w'], size['h'], doScale) )

    s = ''
    if doScale:
        if landscape:
            s = str(size['h'])
        else:
            s = str(size['w'])

    parameters['out_name'] = upUtil.get_output_name(
        videoInfo['filename'], model, opt,
        size_str=s, custom_suffix=suffix, ext=opt['out_videoExt'], customPath=customPath
    )

    depth = videoInfo['depth']
    if depth == 8:
        parameters['out_format'] = 'yuv444p'
        if force_hibit_output:
            parameters['out_format'] = 'yuv444p10'
    else:
        parameters['out_format'] = 'yuv444p{:d}'.format(depth)

    pre_filter = ''

    if noColorConversion:
        out_colorspace = videoInfo['in_colorspace']
        pre_filter = 'setparams=colorspace={}:color_primaries={}:color_trc={}:range={}'.format(
            out_colorspace['matrix'], out_colorspace['primaries'],
            out_colorspace['transfer'], out_colorspace['range'] )
    else:
        if zscale:
            if videoInfo['colorspace'] in ['bt2020', 'bt2020ncl', 'bt2020-10', 'bt2020-12']:
                if depth > 8:
                    logger.info( "Output colorspace: Rec.2020" )
                    #in_color   = 'min=bt2020_ncl:tin=iec61966-2-1:pin=2020:rin=full'
                    out_trc    = '2020_{}'.format(depth)
                    in_color   = 'min=bt2020_ncl:tin={}:pin=2020'.format(out_trc)
                    out_color  = 'p=2020:m=2020_ncl'
                    if fixColor:
                        pre_filter += 'colorspace=all=bt2020:iall=bt2020:fast=1, '  # TODO does this fix colors?
                    logger.warning('Please check for color accuracy.')
                else:
                    logger.info( "Output colorspace: Rec.709" )
                    in_color = 'min=bt2020_ncl:pin=2020:tin=2020_{:d}'.format(depth)
                    out_color  = 'p=709:m=709'
                    out_trc    = '709'
                    if fixColor:
                        pre_filter += 'colorspace=all=bt709:iall=bt470bg:fast=1, '  # fix yellowish colors
            else:
                logger.info( "Output colorspace: Rec.709" )
                in_color = 'min=709:tin=709:pin=709'
                out_color  = 'p=709:m=709'
                out_trc    = '709'
                if fixColor:
                    pre_filter += 'colorspace=all=bt709:iall=bt470bg:fast=1, '  # fix yellowish colors

            final_dither_alg = 'error_diffusion'    # float32 -> int16
            out_color = "t={:s}:d={}:{}".format(out_trc, final_dither_alg, out_color)

            if opt['in_extractFullRange'] or not videoInfo['limitedRange']:
                in_color  += ':rin=full'
                out_color += ':r=full, setrange=full'
                pre_filter += 'setrange=full, '
            else:
                in_color  += ':rin=limited'
                out_color += ':r=limited, setrange=limited'
                pre_filter += 'setrange=limited, '
        else:
            if fixColor:
                #if '_d{}'.format(opt['out_videoExt']) not in videoInfo['filename']:
                pre_filter += 'colorspace=all=bt709:iall=bt470bg:fast=1'

    if lossless:
        encoder = opt['losslessEncoder']
        codec   = opt['losslessCodec']
        if force_hibit_output and opt['losslessEncoder'] == 'nvenc':
            parameters['out_format'] = 'yuv444p16'  # 16 bit needed by nvenc
    else:
        encoder = opt['lossfullEncoder']
        codec   = opt['lossfullCodec']
        if doChromaSub:
            chromaSubType = '420'
            if 'out_ChromaSubsamplingType' in opt:
                chromaSubType = opt['out_ChromaSubsamplingType']
            parameters['out_format'] = parameters['out_format'].replace('444', chromaSubType)
    if '444' in parameters['out_format']:
        chromaSubType = '444'
    elif '422' in parameters['out_format']:
        chromaSubType = '422'
    else:
        chromaSubType = '420'

    parameters['outVideoCodecOptions'] = get_video_codec_options(
        lossless, videoInfo=videoInfo, chromaSubType=chromaSubType, scale=sr_scale, encoder=encoder,
        codec=codec, anime=anime)

    parameters['inAudioOptions'], parameters['outAudioCodecOptions'] = [], []
    if audio_file and isinstance(audio_file, str):
        parameters['inAudioOptions'] = ['-i', audio_file]
        if not upUtil.floatEqual(speedMultiplier, 1):
            opt['forceAudioReencoding'] = True
        parameters['outAudioCodecOptions'] = \
            get_audio_codec_options(videoInfo, opt['out_videoExt'], opt['forceAudioReencoding'])

    post_filter = ''
    if custom_filter != '':
        logger.info('custom_filter: ' + custom_filter)
        post_filter = ', ' + custom_filter

    parameters['scalingOptions'] = []
    if zscale:
        if doScale:
            format_str = pre_filter + 'format=pix_fmts=gbrpf32|yuv444p16'
            in_color_lin  = sub(  ':r=([a-zA-Z]+):',   ':r=linear:', in_color)
            out_color_lin = sub(':rin=([a-zA-Z]+):', ':rin=linear:', in_color)
            parameters['scalingOptions'] = [
                '-vf',
                '{}, zscale={:s}:t=linear:d=none:{:s}, format=pix_fmts=gbrpf32,'
                'zscale=f=lanczos:d=none:w={:d}:h={:d}, zscale={:s}:{:s}{}'.format(
                    format_str, in_color, in_color_lin,
                    size['w'], size['h'],
                    out_color_lin, out_color, post_filter
                )
            ]
        else:
            if parameters['out_format'].find("16") != -1:
                final_dither_alg = 'none'
            parameters['scalingOptions'] = [
                '-vf',
                '{}format=pix_fmts=yuv444p16, zscale={:s}:{:s}{}'.format(
                    pre_filter, in_color, out_color, post_filter
                )
            ]
    else:
        assert not doScale, '!zscale && doScale'
        if pre_filter != '':
            parameters['scalingOptions'] = [ '-vf', '{}{}'.format(pre_filter, post_filter) ]

    if slowDownGIF and videoInfo["fps_float"] > 80:
        videoInfo['fps'] = opt['out_slowDownGIF_FPS']
        update_videoInfoFPS(videoInfo)
        fpsStr = str(videoInfo["fps_float"])
    else:
        fpsStr = str(videoInfo["fps"] if videoInfo["fps"] else videoInfo["fps_float"])

    parameters['fpsStr_out'] = fpsStr
    parameters['fpsStr_in'] = parameters['fpsStr_out']

    return parameters


def create_video(frameDir, videoInfo, audio_file, opt, model='', suffix='', custom_filter='',\
                 anime=False, speedMultiplier=1, useZscale=False, fixColor=True, customPath=None):
    logger = logging.getLogger('base')

    params = get_video_parameters(videoInfo, audio_file, opt, model, suffix,
                                  custom_filter, anime, speedMultiplier,
                                  zscale=useZscale, fixColor=fixColor, customPath=customPath)

    cmd = [
        'ffmpeg', '-hide_banner',
        '-thread_queue_size', '4096',
        #'-loglevel', 'verbose',
        '-sws_flags', '+accurate_rnd+full_chroma_inp',
        '-f', 'image2',
        '-framerate', params['fpsStr_in'],
        '-pattern_type', 'glob',
        '-i', '{:s}/*{:s}'.format(frameDir, opt['output_ext']),
        *params['inAudioOptions'],
        *params['scalingOptions'],
        '-pix_fmt', params['out_format'],
        '-r', params['fpsStr_out'],
        '-aspect', str(params['aspect_ratio']),
        *params['outVideoCodecOptions'],
        *params['outAudioCodecOptions'],
        params['out_name']
    ]
    logger.info(cmd)
    subprocess.run(cmd, check=True)

    return params['out_name']


class ImmediateVideoCreator():
    """ Streams single video frames to ffmpeg for continuing encoding. """

    def __init__(self, videoInfo, audioFile, opt, model='', suffix='',
                 custom_filter='', anime=False, useZscale=False):
        self.hasAudio = videoInfo['hasAudio'] and audioFile

        self.params = get_video_parameters(videoInfo, audioFile, opt, model, suffix,
                                           custom_filter, anime,
                                           zscale=useZscale)

        self.outName = self.params['out_name']
        self.audioFile = audioFile
        self.setupDone = False
        self.encoder = None

    def setup(self, frame):
        logger = logging.getLogger('base')

        out_videoEncoder =  self.params['outVideoCodecOptions'][1]

        out_videoOptions =  self.params['outVideoCodecOptions'][2:]
        customOptions = {}
        for i, entry in enumerate(out_videoOptions):
            if entry.startswith('-'):
                customOptions[entry[1:]] = out_videoOptions[i+1]
        for i, entry in enumerate(self.params['scalingOptions']):
            if entry.startswith('-'):
                customOptions[entry[1:]] = self.params['scalingOptions'][i+1]
        logger.info({'customOptions': customOptions})

        out_width = frame.width
        out_height = frame.height
        in_fps =  self.params['fpsStr_in']
        out_fps =  self.params['fpsStr_out']
        sizeStr = '{:d}x{:d}'.format(round(out_width), round(out_height))

        logger.debug('ffmpeg.input: s={}, framerate={}'.format(sizeStr, in_fps))
        video = ffmpeg.input('pipe:', format='rawvideo', pix_fmt='rgb48',
                             s=sizeStr, framerate=in_fps,
                             sws_flags='+accurate_rnd+full_chroma_inp')

        logger.debug('ffmpeg.output: {}, pix_fmt={}, c:v={}, r={}, aspect={}, {}'.format(
            self.outName, self.params['out_format'], out_videoEncoder, out_fps,
            str(self.params['aspect_ratio']), customOptions) )

        if self.hasAudio:
            audio = ffmpeg.input(self.audioFile).audio
            print(audio)
            self.encoder = ( ffmpeg.output(video, audio, self.outName,
                                           pix_fmt=self.params['out_format'],
                                           vcodec=out_videoEncoder,
                                           loglevel='info', acodec='copy', r=out_fps,
                                           aspect=str(self.params['aspect_ratio']),
                                           **customOptions )
                             .run_async(pipe_stdin=True, overwrite_output=True) )
        else:
            self.encoder = ( ffmpeg.output(video, self.outName,
                                           pix_fmt=self.params['out_format'],
                                           vcodec=out_videoEncoder,
                                           loglevel='info', acodec='copy', r=out_fps,
                                           aspect=str(self.params['aspect_ratio']),
                                           **customOptions )
                             .run_async(pipe_stdin=True, overwrite_output=True) )
        self.setupDone = True
        logger.debug(self.params)

    def add_frame(self, frame):
        if not self.setupDone:
            self.setup(frame)

        # <- wand Image float32, RGB
        inputData = numpy.array(frame.export_pixels(channel_map='RGB', storage='short'),
                                dtype=numpy.uint16)
        inputData = inputData.reshape(frame.height, frame.width, 3)
        # -> Numpy float32, HWC, RGB

        self.encoder.stdin.write(inputData)

    def finish(self):
        self.encoder.stdin.close()
        self.encoder.wait()


def encode_video(
        in_videoFileName, encoder=auto, codec=auto, out_videoFileName='', ext='.mp4',
        customInputOptions=[], customOutputOptions=[],
        doKeepHighBitrate=False, doChromaSubsampling=True, doLosslessEncoding=False):
    logger = logging.getLogger('base')
    logger.info(in_videoFileName)
    inputDict = {'encoder': encoder, 'codec': codec, 'out_videoFileName': out_videoFileName,
                 'ext': ext,
                 'customInputOptions': customInputOptions,
                 'customOutputOptions': customOutputOptions,
                 'doKeepHighBitrate': doKeepHighBitrate,
                 'doChromaSubsampling': doChromaSubsampling,
                 'doLosslessEncoding': doLosslessEncoding }
    logger.debug('encode_video({})'.format(inputDict))

    videoInfo = get_video_info(in_videoFileName)
    depth = videoInfo['depth']
    highBitrate = doKeepHighBitrate and depth > 8
    logger.debug({'highBitrate': highBitrate})
    videoFormat = videoInfo['format']
    fullFormat = '444' in videoFormat or 'rgb' in videoFormat or 'bgr' in videoFormat
    logger.debug({'fullFormat': fullFormat})

    if 'grey' in videoFormat:
        raise NotImplementedError("Grey formats not implemented yet.")

    audioCodecOptions = get_audio_codec_options(videoInfo, ext)
    logger.debug( 'audioStr: "{}"'.format(audioCodecOptions) )

    formatOptions = []
    if doChromaSubsampling:
        if fullFormat or '422' in videoFormat:
            formatOptions = ['-pix_fmt', 'yuv420p']
        if codec == auto and encoder == auto:   # probably default settings
            if videoInfo['shortSide'] <= 1080:  # no chroma subsampling for res. lower than fhd
                formatOptions = []
    logger.debug({'ChromaSubsamplingStr': formatOptions})

    if highBitrate:
        if formatOptions == []:
            formatOptions = ['-pix_fmt', videoFormat]
        else:
            formatOptions += '{:d}'.format(depth)
        logger.debug({'highBitrateStr': formatOptions})
    else:
        # convert to 8 bit
        if formatOptions == [] and not doChromaSubsampling:
            formatOptions = ['-pix_fmt']
            if fullFormat:
                formatOptions.append('yuv444p')
            elif '422' in videoFormat:
                formatOptions.append('yuv422p')
            else:
                formatOptions.append('yuv420p')
        logger.debug({'lowBitrateStr': formatOptions})
    if len(formatOptions) > 1 and '444' in formatOptions[1]:
        chromaSubType = '444'
    elif len(formatOptions) > 1 and '422' in formatOptions[1]:
        chromaSubType = '422'
    else:
        chromaSubType = '420'

    videoCodecOptions = get_video_codec_options(doLosslessEncoding, videoInfo=videoInfo,
                                                fullChroma=fullChroma, encoder=encoder,
                                                codec=codec)

    if out_videoFileName == '':
        out_videoFileName = upUtil.get_output_name(
            in_videoFileName, custom_suffix=('_' + codec), ext=ext )
    in_videoFileName = upUtil.escape_filename_ffmpeg(in_videoFileName)

    if '444' in videoFormat or 'rgb' in videoFormat:
        chromaFlagStr = 'full_chroma_inp'
    else:
        chromaFlagStr = 'full_chroma_int'

    cmd = [
        'ffmpeg', '-hide_banner',
        '-thread_queue_size', '4096',
        '-sws_flags', '+accurate_rnd+' + chromaFlagStr,
        *customInputOptions,
        '-i', in_videoFileName,
        *formatOptions,
        *customOutputOptions,
        *videoCodecOptions,
        *audioCodecOptions,
        out_videoFileName ]
    logger.info(cmd)
    subprocess.run(cmd, check=True)


def concat_videos(in_videoFileNames, in_audioFile, out_videoFileName):
    logger = logging.getLogger('base')
    logger.info( 'Merging {:d} split videos...'.format( len(in_videoFileNames) ) )
    logger.debug( 'concat_videos({}, {}, {})'.format(
        in_videoFileNames, in_audioFile, out_videoFileName
    ) )

    concatLines = []
    for i, vfn in enumerate(in_videoFileNames):
        vfn = upUtil.escape_filename_ffmpeg(vfn)
        concatLines.append( "file '{:s}'\n".format(vfn) )
    logger.debug( 'concatLines: {}'.format(concatLines) )

    concatFileName = osp.join(osp.split(out_videoFileName)[0], 'concatFile')
    with open(concatFileName, 'w') as concatFile:
        concatFile.writelines(concatLines)

    in_audioOptions = out_audioOptions = []
    if in_audioFile and isinstance(in_audioFile, str):
        in_audioOptions  = ['-i', in_audioFile]
        out_audioOptions = ['-c:a', 'copy']

    doLosslessEncoding = True   # otherwise, we'd need videoInfo, fullChroma, codec
    out_VideoCodecOptions = get_video_codec_options(doLosslessEncoding)

    cmd = [
        'ffmpeg', '-hide_banner',
        '-f', 'concat',
        '-safe', '0',
        '-i', concatFileName,
        *in_audioOptions,
        *out_audioOptions,
        *out_VideoCodecOptions,
        upUtil.escape_filename_ffmpeg(out_videoFileName) ]
    logger.info(cmd)

    subprocess.run(cmd, check=True)

    for vfn in in_videoFileNames:
        remove(vfn)
    remove(concatFileName)


def create_rotation_filter(rotation, videoInfo):
    """ Creates a transpose filter to rotate a video.
        NOTE: Does not alter aspect ratio. This needs to be done by the caller. """
    assert rotation in ['clock', 'cclock',
                        'clock_flip', 'cclock_flip'], 'Unsupported rotation {}'.format(rotation)

    filterStr = 'transpose=dir={}'.format(rotation)

    if not upUtil.floatEqual(videoInfo['aspect_ratio'], 1):
        videoInfo['aspect_ratio'] = 1 / videoInfo['aspect_ratio']
        videoInfo['width'], videoInfo['height'] = videoInfo['height'], videoInfo['width']
        evaluate_aspect_ratio(videoInfo)

    return filterStr
